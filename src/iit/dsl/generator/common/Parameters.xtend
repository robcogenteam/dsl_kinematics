package iit.dsl.generator.common

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.ParametersDeclaration
import iit.dsl.coord.generator.ParameterFQN
import iit.dsl.generator.Common
import iit.dsl.generator.Common.IPField
import iit.dsl.generator.Common.JFField
import iit.dsl.kinDsl.AbstractLink
import iit.dsl.kinDsl.Expr
import iit.dsl.kinDsl.FloatLiteral
import iit.dsl.kinDsl.Identifier
import iit.dsl.kinDsl.InertiaParams
import iit.dsl.kinDsl.Joint
import iit.dsl.kinDsl.ParameterLiteral
import iit.dsl.kinDsl.RefFrame
import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.Var
import iit.dsl.kinDsl.Vector3
import java.util.HashMap
import java.util.HashSet
import java.util.Map
import java.util.Set

/**
 * Utilities related to the parameters (i.e. non-constants) possibly included
 * in the robot description files.

 * Parameters associated with different robot properties are recognized as
 * different, even though they share the same name. However, the code generators
 * might still use the name as the identifier in code, which would lead to
 * clashes. Anyway, using the same name for different robot properties is
 * probably a bad practice in the first place, so try to avoid it.
 */
class Parameters
{
    public static abstract class ParameterInfo
    {
        def public String name()         { return emfObj.varname }
        def public ParameterLiteral emfObj() { return cast( modelvar() ) }
        def public boolean isKinematic() { return false }
        def public boolean isInertial()  { return false }
        def public boolean isJoint()     { return false }
        def public boolean isLink ()     { return false }
        def public boolean isUserFrame() { return false }

        def package Var modelvar()
    }
    public static abstract class KinematicParameter extends ParameterInfo
    {
        new(JFField f) { field=f; }
        override public isKinematic() { return true }
        def public boolean isRotation()    { return common.isRotation(field) }
        def public boolean isTranslation() { return !isRotation() }
        public final JFField field
    }
    public static class JointParameter extends KinematicParameter
    {
        new(Joint j, JFField f) { super(f); joint=j; }
        override public isJoint() { return true }
        public final Joint   joint
        override modelvar() { return common.getVar(joint,field) }

        override equals(Object rhs) {
            if(rhs===null) return false
            if(JointParameter != rhs.getClass) return false
            val other = rhs as JointParameter
            return (joint.equals(other.joint) && field.equals(other.field))
        }
        override hashCode() {
            return 53*joint.hashCode + 131*field.hashCode
        }
    }
    public static class FrameParameter extends KinematicParameter
    {
        new(RefFrame fr, AbstractLink l, JFField f) { super(f); frame=fr; link=l; }
        override public isUserFrame() { return true }
        public final RefFrame     frame
        public final AbstractLink link
        override modelvar() { return common.getVar(frame,field) }

        override equals(Object rhs) {
            if(rhs===null) return false
            if(FrameParameter != rhs.getClass) return false
            val other = rhs as FrameParameter
            return (frame.equals(other.frame) && link.equals(other.link) && field.equals(other.field))
        }
        override hashCode() {
            return 53*joint.hashCode + 17*link.hashCode + 131*field.hashCode
        }
    }
    public static class LinkParameter extends ParameterInfo
    {
        new(AbstractLink l, IPField f) { link=l; field=f }
        override public isInertial() { return true }
        override public isLink()     { return true }
        public final AbstractLink link
        public final IPField      field
        override modelvar(){ return common.getVar(link,field) }

        override equals(Object rhs) {
            if(rhs===null) return false
            if(LinkParameter != rhs.getClass) return false
            val other = rhs as LinkParameter
            return (link.equals(other.link) && field.equals(other.field))
        }
        override hashCode() {
            return 53*link.hashCode + 131*field.hashCode
        }
    }

    new(Robot rob)
    {
        robot   = rob
        for(j : robot.joints) {
            lengths.addAll(  trParametersOf(j) )
            angles .addAll( rotParametersOf(j) )
        }
        kinIsParametric = (lengths.size>0 || angles.size>0)
        for(l : robot.linksAndBase) {
            val ret = parametersOf(l.inertiaParams)
            inertias.addAll( ret.key )
            iFlags.put(l, ret.value)
            for( fr : l.frames ) {
                lengths.addAll(  trParametersOf(fr,l) )
                angles .addAll( rotParametersOf(fr,l) )
            }
        }
        // now populate the map indexed with the model Var (EMF object from the grammar)
        for(l : lengths) {
            byVar.put( l.modelvar, l)
        }
        for(a : angles) {
            byVar.put( a.modelvar, a)
        }
        for(i : inertias) {
            byVar.put( i.modelvar, i)
        }
    }

    def public boolean isParametric() {
        return (framesAreParametric || inertiaIsParametric)
    }
    /**
     * @return true if there is at least one frame (joint or user-defined) that
     *         has at least one parameter in its pose specification;
     *         false otherwise.
     */
    def public boolean framesAreParametric() {
        return (lengths.size > 0 || angles.size > 0)
    }
    /**
     * @return true if there is at least one Joint frame which has at least one
     *         parameter in its pose specification; false otherwise.
     */
    def public boolean kinematicsIsParametric() {
        return kinIsParametric
    }
    /**
     * @return true if there is at least one parameter in the robot model
     *         associated with an inertia property; false otherwise.
     */
    def public boolean inertiaIsParametric() {
        return inertias.size > 0
    }

    def public Set<ParameterInfo> lengths () { return this.lengths }
    def public Set<ParameterInfo> angles  () { return this.angles }
    def public Set<ParameterInfo> inertias() { return this.inertias }
    /**
     * A map from a link to the flags signaling which property is parametric
     */
    def public Map<AbstractLink,ParametricInertiaFlags> parametricInertiaFlagsMap() { return iFlags }


    public final Robot robot
    private Set<ParameterInfo> lengths  = new HashSet<ParameterInfo>()
    private Set<ParameterInfo> angles   = new HashSet<ParameterInfo>()
    private Set<ParameterInfo> inertias = new HashSet<ParameterInfo>()
    private Map<AbstractLink,ParametricInertiaFlags> iFlags = new HashMap<AbstractLink, ParametricInertiaFlags> ()
    private boolean kinIsParametric = false
    public final Map<Var, ParameterInfo> byVar = new HashMap<Var, ParameterInfo>

    def public static Set<JointParameter> trParametersOf(Joint joint)
    {
        val ret = new HashSet<JointParameter>()
        val tr = joint.refFrame.translation
        if( isParameter(tr.x) ) {
            ret.add( new JointParameter(joint, JFField.TX) )
        }
        if( isParameter(tr.y) ) {
            ret.add( new JointParameter(joint, JFField.TY) )
        }
        if( isParameter(tr.z) ) {
            ret.add( new JointParameter(joint, JFField.TZ) )
        }
        return ret
    }
    def public static Set<JointParameter> rotParametersOf(Joint joint)
    {
        val ret = new HashSet<JointParameter>()
        val rot = joint.refFrame.rotation
        if( isParameter(rot.x) ) {
            ret.add( new JointParameter(joint, JFField.RX) )
        }
        if( isParameter(rot.y) ) {
            ret.add( new JointParameter(joint, JFField.RY) )
        }
        if( isParameter(rot.z) ) {
            ret.add( new JointParameter(joint, JFField.RZ) )
        }
        return ret
    }
    def public static Set<FrameParameter> trParametersOf(RefFrame fr, AbstractLink link)
    {
        val ret = new HashSet<FrameParameter>()
        val tr = fr.transform.translation
        if( isParameter(tr.x) ) {
            ret.add( new FrameParameter(fr, link, JFField.TX) )
        }
        if( isParameter(tr.y) ) {
            ret.add( new FrameParameter(fr, link, JFField.TY) )
        }
        if( isParameter(tr.z) ) {
            ret.add( new FrameParameter(fr, link, JFField.TZ) )
        }
        return ret
    }
    def public static Set<FrameParameter> rotParametersOf(RefFrame fr, AbstractLink link)
    {
        val ret = new HashSet<FrameParameter>()
        val rot = fr.transform.rotation
        if( isParameter(rot.x) ) {
            ret.add( new FrameParameter(fr, link, JFField.RX) )
        }
        if( isParameter(rot.y) ) {
            ret.add( new FrameParameter(fr, link, JFField.RY) )
        }
        if( isParameter(rot.z) ) {
            ret.add( new FrameParameter(fr, link, JFField.RZ) )
        }
        return ret
    }
    /**
     * The set of parameters (ie non-constant values) contained in the given
     * inertia properties.
     */
    def public static Pair<Set<LinkParameter>, ParametricInertiaFlags>
                                            parametersOf(InertiaParams inertia)
    {
        val set = new HashSet<LinkParameter>
        val link= inertia.eContainer as AbstractLink
        val flags = new ParametricInertiaFlags

        if( flags.mass = isParameter(inertia.mass) ) {
            set.add( new LinkParameter(link, IPField.MASS) )
        }
        if( flags.com = isParameter(inertia.com.x) ) {
            set.add( new LinkParameter(link, IPField.COMX) )
        }
        if( isParameter(inertia.com.y) ) {
            set.add( new LinkParameter(link, IPField.COMY) )
            flags.com = true
        }
        if( isParameter(inertia.com.z) ) {
            set.add( new LinkParameter(link, IPField.COMZ) )
            flags.com = true
        }
        if( flags.tensor = isParameter(inertia.ix) ) {
            set.add( new LinkParameter(link, IPField.IX) )
        }
        if( isParameter(inertia.iy) ) {
            set.add( new LinkParameter(link, IPField.IY) )
            flags.tensor = true
        }
        if( isParameter(inertia.iz) ) {
            set.add( new LinkParameter(link, IPField.IZ) )
            flags.tensor = true
        }
        if( isParameter(inertia.ixy) ) {
            set.add( new LinkParameter(link, IPField.IXY) )
            flags.tensor = true
        }
        if( isParameter(inertia.ixz) ) {
            set.add( new LinkParameter(link, IPField.IXZ) )
            flags.tensor = true
        }
        if( isParameter(inertia.iyz) ) {
            set.add( new LinkParameter(link, IPField.IYZ) )
            flags.tensor = true
        }

        return new Pair(set, flags)
    }

    /**
     * \name Parameters-group names
     *
     * The name of the group containing the length/angular parameters of a
     * robot.
     * Such a name will be used in the document of the Motion-DSL and therefore
     * in the one of the Transforms-DSL related to a robot, and will appear
     * also in the generated code (eg as the type of the C++ struct containing
     * the actual values of the parameters)
     */
    ///@{
    def public static String lengthsGroupName() { "lengths" }
    def public static String anglesGroupName()  { "angles" }
    ///@}

    def public static ParameterLiteral asParameter(Var v) {
        if( isParameter(v) ) {
            return cast(v)
        }

        if(v instanceof FloatLiteral) {
            throw new RuntimeException("Cannot cast the constant " +
                v.value + " to a ParameterLiteral")
        }
        if(v instanceof Expr) {
            throw new RuntimeException("Cannot cast the expression " +
                v.identifier.varname + " to a ParameterLiteral")
        }
        throw new RuntimeException("Could not cast the given variable " +
                "to a ParameterLiteral")
    }

    /**
     * Tells whether one or more components of the given vector is a user
     * parameter
     */
    def static boolean isParametric(Vector3 vec) {
        return isParameter(vec.x) || isParameter(vec.y) || isParameter(vec.z)
    }

    def public static toTransformsDSLParameter(Model transforms, ParameterInfo p)
    {
        if( p.isKinematic() ) {
            val pkin = p as KinematicParameter
            if( pkin.isRotation ) {
                return convertAngleParam(transforms, pkin.emfObj)
            } else {
                return convertLengthParam(transforms, pkin.emfObj)
            }
        }
        return null
    }

    /**
     * Converts a length parameter of the Kinematics-DSL to one of the
     * Transforms-DSL
     */
    def public static iit.dsl.coord.coordTransDsl.ParameterLiteral
        convertLengthParam(
            Model transforms,
            ParameterLiteral param)
    {
        val fqn = new ParameterFQN
        fqn.groupName = lengthsGroupName
        fqn.paramName = param.varname
        return getParam(transforms, fqn)
    }
    /**
     * Converts an agular parameter of the Kinematics-DSL to one of the
     * Transforms-DSL
     */
    def public static iit.dsl.coord.coordTransDsl.ParameterLiteral
        convertAngleParam(
            Model transforms,
            ParameterLiteral param)
    {
        val fqn = new ParameterFQN
        fqn.groupName = anglesGroupName
        fqn.paramName = param.varname
        return getParam(transforms, fqn)
    }

    /**
     * Returns a ParameterLiteral of the Transforms-DSL, given a model and
     * the fully qualified name of the parameter itself
     */
    def public static iit.dsl.coord.coordTransDsl.ParameterLiteral
        getParam(
            Model transforms,
            ParameterFQN fqn)
    {
        return iit.dsl.coord.generator.Common::getParameterFromName(transforms, fqn)
    }

    /**
     * \name Parameters-group getters
     * These methods return the iit.dsl.coord.coordTransDsl.ParametersDeclaration
     * instance that corresponds either to the length or the angular parameters
     * of the robot.
     */
    ///@{
    def public static ParametersDeclaration
        getLengthParamsGroup(Model transforms)
    {
        iit.dsl.coord.generator.Common::getParametersGroupFromName(transforms, lengthsGroupName)
    }
    def public static ParametersDeclaration
        getAngleParamsGroup(Model transforms)
    {
        iit.dsl.coord.generator.Common::getParametersGroupFromName(transforms, anglesGroupName)
    }
    ///@}


    def public static boolean isParametric(InertiaParams inertia) {
        return isParameter(inertia.mass) ||
                isParametric(inertia.com) ||
                isParameter(inertia.ix)||isParameter(inertia.iy)||
                isParameter(inertia.iz)||isParameter(inertia.ixy)||
                isParameter(inertia.ixz)||isParameter(inertia.iyz)
    }

    def public static ParametricInertiaFlags whoIsParametric(InertiaParams inertia)
    {
        val ret = new ParametricInertiaFlags
        ret.mass = isParameter(inertia.mass)
        ret.com  = isParametric(inertia.com)
        ret.tensor = isParameter(inertia.ix) || isParameter(inertia.iy) ||
                     isParameter(inertia.iz) || isParameter(inertia.ixy)||
                     isParameter(inertia.ixz)|| isParameter(inertia.iyz)
        return ret
    }


    def public static dispatch boolean isParameter(Var x) { false }
    def public static dispatch boolean isParameter(Expr x) { isParameter(x.identifier) }
    def public static dispatch boolean isParameter(Identifier x) { false }
    def public static dispatch boolean isParameter(ParameterLiteral x) { true }

    def private static ParameterLiteral cast(Var x) {
        return ((x as Expr).identifier as ParameterLiteral)
    }

    private static extension Common common = Common.instance
}

class ParametricInertiaFlags {
    public boolean mass;
    public boolean com;
    public boolean tensor;
    def public boolean any() { return mass || com || tensor }
    def public boolean none(){ return !any() }
    def public boolean all() { return mass && com && tensor }
}


