package iit.dsl.generator.common

import iit.dsl.kinDsl.Robot
import java.util.HashMap
import java.util.Map

/**
 * A global register of the currently loaded robot models and additional data
 * associated with them.
 *
 * This register allows to retrieve common metadata objects attached to a Robot
 * instance, to relieve clients from constructing the metadata themselves, thus
 * avoiding replication.
 *
 * Such metadata classes include some of the classes in this package, for
 * example the container of the parameters of a robot model.
 */
class RobotInfoRegister
{
    def public static parameters(Robot r) {
        var Parameters ret = parameters.get(r)
        if(ret === null) {
            ret = new Parameters(r)
            parameters.put(r, ret)
        }
        return ret
    }
    def public static constants(Robot r) {
        var Constants ret = constants.get(r)
        if(ret === null) {
            ret = new Constants(r)
            constants.put(r, ret)
        }
        return ret
    }

    private static Map<Robot, Parameters> parameters = new HashMap<Robot, Parameters>
    private static Map<Robot, Constants>  constants  = new HashMap<Robot, Constants>
}