package iit.dsl.generator.common

import iit.dsl.kinDsl.Robot
import iit.dsl.generator.common.Constants.ConstantInfo

import org.eclipse.xtend2.lib.StringConcatenation

/**
 * A generator of the expressions (in a target language) that would resolve to
 * the value of a model constant.
 *
 * In fact, the actual expression (i.e. the code text) is requested to a
 * configurator, which keeps the class independent of the language.
 * This class really deals with the constant-folding option: if enabled, the
 * expression for a constant is a number literal; otherwise, the configurator
 * is queried.
 *
 * This class also provides a factory method for an implementation of the interface
 * IConstantsAccess required by code generators of the Transforms-DSL.
 */
class ConstantsAccess
{
    public static enum Func { NONE, SINE, COSINE }

    public static interface IConfigurator
    {
        /**
         * The expression (in the target language) that shall evaluate to the
         * value of the given constant, assuming that constant folding is
         * disabled (that is, a constant is not replaced with the corresponding
         * number literal)
         */
        def String valueExpression(ConstantInfo c, Func f);

        def String definitionExpression(ConstantInfo c, Func f)
    }

    new(Robot robot, boolean cFolding, IConfigurator cfg)
    {
        this.robot  = robot
        this.robotConsts = RobotInfoRegister.constants(robot)
        this.constantFolding = cFolding
        this.langspecs = cfg
    }

    def public foldingEnabled() { return constantFolding }

    def public valueExpr(ConstantInfo constantInfo) {
        val actual = getActualConstant( constantInfo )
        return expression(actual, Func.NONE)
    }
    def public valueExpr(ConstantInfo constantInfo, Func func) {
        val actual = getActualConstant( constantInfo )
        return expression(actual, func)
    }
    /* In case the inertia properties in the robot model are expressed with a
     * custom frame, the robot constant associated with any of those property
     * is not the value that we want; because we want the value of inertia
     * properties in body coordinates.
     * Thus, when the user is passing a ConstantInfo here, we look for the
     * actual constant with the same name in the robot metadata, as that
     * container has the inertia properties in the right frame (see
     * OverridenLinkConstant).
     */
    def private ConstantInfo getActualConstant(ConstantInfo userToken) {
        val actual = robotConsts.byName.get( userToken.name )
        if (actual !== null) return actual
        return userToken
    }

    /**
     * Creates the code with the list of definitions of each constant of the
     * robot model.
     */
    def public CharSequence allDefinitions()
    {
        val code = new StringConcatenation()
        for( jc : robotConsts.rotations )
        {
            if( ! jc.isPIExpr ) {
                code.append( langspecs.definitionExpression(jc, Func.NONE) )
                code.newLine()
                code.append( langspecs.definitionExpression(jc, Func.SINE) )
                code.newLine()
                code.append( langspecs.definitionExpression(jc, Func.COSINE) )
                code.newLine()
            }
        }
        for( tr : robotConsts.lengths ) {
            code.append( langspecs.definitionExpression( tr, Func.NONE ) )
            code.newLine()
        }
        for( ic : robotConsts.inertias ) {
            code.append( langspecs.definitionExpression( ic, Func.NONE ) )
            code.newLine()
        }
        return code
    }

    def public iit.dsl.coord.generator.cpp.IConstantsAccess makeCTDSLConstantsAccess()
    {
        return new _inner(this)
    }

    def private String expression(ConstantInfo info, Func func)
    {
        var double value = info.value
        if( value == 0.0 ) {
            return "0.0"
        }
        if( info.isPIExpr ) {
            // We always fold in the numerical value of PI expressions
            // TODO ask the configurator for lang-specific expressions for PI expressions, to postpone as much as possible numerical truncations
            switch( func ) {
                case SINE:  value = Math.sin(value)
                case COSINE:value = Math.cos(value)
                case NONE: {}
            }
            return value.toString
        }
        if( constantFolding ) {
            // return the float-value literal
            switch( func ) {
                case SINE:  value = Math.sin(value)
                case COSINE:value = Math.cos(value)
                case NONE: {}
            }
            return value.toString
        }

        // The property is necessarily a non-zero constant, and we do not want
        // constant folding; therefore return the expression that resolves to
        // its value
        return langspecs.valueExpression(info, func)
    }

    public final Robot     robot
    public final Constants robotConsts
    private boolean constantFolding = false
    private IConfigurator langspecs = null


    private static class _inner implements iit.dsl.coord.generator.cpp.IConstantsAccess
    {
        public new(ConstantsAccess core) {
            this.outer = core
        }

        override valueExpression(iit.dsl.coord.coordTransDsl.Model    model,
                             iit.dsl.coord.coordTransDsl.Constant c)
        {
            val constInfo = infoByName( c.name )
            return outer.valueExpr(constInfo)
        }

        override String valueExpression(iit.dsl.coord.coordTransDsl.Model    model,
                                 iit.dsl.coord.coordTransDsl.Constant c,
                                 String function)
        {
            val constInfo = infoByName( c.name )

            if( "sine".equals(function) ) {
                return outer.valueExpr( constInfo, Func.SINE )
            }
            if( "cosine".equals(function) ) {
                return outer.valueExpr( constInfo, Func.COSINE )
            }
            return "// ERROR in iit.dsl.generator.ConstantsAccess : unknown function " + function
        }

        def private infoByName(String name)
        {
            val info = outer.robotConsts.byName.get( name )
            if( info === null ) {
                throw new RuntimeException("Could not resolve constant '" + name +
                                                 "' for robot '" + outer.robot.name + "'")
            }
            if( info.isLink() ) {
                // this should never happen, a constant in the transforms-model cannot
                //  be the value of some inertia property
                throw new RuntimeException("Constant " + name + " is an inertia property - a kinematic constant was expected")
            }
            return info
        }

        private final ConstantsAccess outer
    }
}