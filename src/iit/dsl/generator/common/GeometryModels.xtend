package iit.dsl.generator.common

import iit.dsl.kinDsl.Robot
import iit.dsl.generator.MotionDSLDocGenerator

import iit.dsl.motiondsl.motionDsl.Model
import iit.dsl.motiondsl.generator.CoordTransDSL_generator
import iit.dsl.coord.generator.Utilities.MatrixConvention
import iit.dsl.transspecs.transSpecs.DesiredTransforms
import java.io.IOException

/**
 * A unique access point to the Motions-DSL and Transforms-DSL models associated
 * with the geometric properties of a given robot.
 *
 * This class is constant-folding aware, in the sense that different geometry
 * models are returned, in general, depending on the constant-folding option.
 *
 * If constant folding is enabled, the numerical constants of the joint poses in
 * the robot model are copied, as float literals, into the geometry models.
 * Otherwise, the models will contain symbolic placeholders, and the code
 * generated out of these models will be different.
 */
class GeometryModels
{
    public new (Robot rob, boolean cfolding)
    {
        robot = rob
        constantFolding = cfolding
        motionsDoc = motionDocGen.documentContent(robot, constantFolding)
        motions = motionDSL.getModel( motionsDoc.toString )
    }

    def public getRobot()        { return robot   }
    def public getMotionsDoc()   { return motionsDoc }
    def public getMotionsModel() { return motions }

    /**
     * Content of a document of the Transforms-DSL, with the specification of
     * the default coordinate transforms for the robot of this instance.
     */
    def public getTransformsDoc() {
        return transformsDocGen.generateDSLDoc(motions, MatrixConvention.TRANSFORMED_FRAME_ON_THE_RIGHT)
    }
    /**
     * Content of a document of the Transforms-DSL, with the specification of
     * the coordinate transforms for the robot of this instance.
     *
     * @param query the transforms to be included in the returned document
     */
    def public getTransformsDoc(DesiredTransforms query) {
        return transformsDocGen.generateDSLDoc(motions, MatrixConvention.TRANSFORMED_FRAME_ON_THE_RIGHT, query)
    }
    /**
     * Creates and returns the model of the Coordinate-Transforms DSL
     * for the robot of this instance.
     * @return the model of the coordinate transforms for the robot, including
     *         default ones and those specified by the argument.
     */
    def public getTransformsModel(DesiredTransforms query) throws IOException {
        return transformsDSL.getModel( getTransformsDoc(query).toString() )
    }
    /**
     * Creates and returns the model of the Coordinate-Transforms DSL
     * for the robot of this instance.
     * @return the model of the default coordinate transforms for the robot
     */
    def public getTransformsModel() throws IOException {
        return transformsDSL.getModel( getTransformsDoc().toString() )
    }

    private final Robot   robot
    private final boolean constantFolding
    private final CharSequence motionsDoc
    private final Model   motions

    private static final iit.dsl.motiondsl.utils.DSLAccessor motionDSL     = new iit.dsl.motiondsl.utils.DSLAccessor()
    private static final iit.dsl.coord.utils.DSLAccessor     transformsDSL = new iit.dsl.coord.utils.DSLAccessor()
    private static final MotionDSLDocGenerator   motionDocGen     = new MotionDSLDocGenerator()
    private static final CoordTransDSL_generator transformsDocGen = new CoordTransDSL_generator()
}