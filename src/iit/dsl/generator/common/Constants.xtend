package iit.dsl.generator.common

import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.Expr
import iit.dsl.kinDsl.FloatLiteral
import iit.dsl.kinDsl.PILiteral
import iit.dsl.kinDsl.ParameterLiteral
import iit.dsl.kinDsl.AbstractLink
import iit.dsl.kinDsl.Joint
import iit.dsl.kinDsl.RotoTrasl
import iit.dsl.kinDsl.RefFrame
import iit.dsl.kinDsl.InertiaParams
import iit.dsl.kinDsl.Var

import iit.dsl.generator.Common
import iit.dsl.generator.Common.IPField
import iit.dsl.generator.Common.JFField

import java.util.ArrayList
import java.util.List
import java.util.Map
import java.util.HashMap

/**
 * A container of all the non-zero constants of a Robot.
 *
 * The class also provides some static utility methods to deal with model
 * constants.
 */
class Constants
{
    public static abstract class ConstantInfo
    {
        def public String name();
        def public boolean isJoint()     { return false }
        def public boolean isLink ()     { return false }
        def public boolean isUserFrame() { return false }
        def public boolean isPIExpr()    { return false }
        def public double  value()       { return utils.asFloat( modelvar() ) }

        def public Var    modelvar();

        def package check() {
            if (modelvar() !== null) {
                if( ! modelvar().isConst ) {
                    throw new RuntimeException("Cannot build a ConstantInfo from a non-constant robot model attribute")
                }
            }
        }
    }
    public static abstract class KinematicConstant extends ConstantInfo
    {
        new(JFField f) { field=f; }
        def public boolean isRotation()    { return utils.isRotation(field) }
        def public boolean isTranslation() { return !isRotation() }
        override isPIExpr()    { return modelvar.PIExpr }
        public final JFField field
    }
    public static class JointConstant extends KinematicConstant
    {
        new(Joint j, JFField f) { super(f); joint=j; check() }
        override name()    { return name(joint,field) }
        override isJoint() { return true }
        public final Joint   joint

        override public modelvar(){ return utils.getVar(joint,field) }
    }
    public static class FrameConstant extends KinematicConstant
    {
        new(RefFrame fr, AbstractLink l, JFField f) { super(f); frame=fr; link=l; check() }
        override public name()    { return name(frame,field) }
        override public isUserFrame() { return true }
        public final RefFrame     frame
        public final AbstractLink link

        override public modelvar(){ return utils.getVar(frame,field) }
    }
    public static class LinkConstant extends ConstantInfo
    {
        new(AbstractLink l, IPField f) { link=l; field=f; check() }
        override public name()    { return name(link,field) }
        override public isLink() { return true }
        public final AbstractLink link
        public final IPField      field

        override public modelvar(){ return utils.getVar(link,field) }
    }
    /* A little hack to cope with inertia properties in custom frames.
     * The regular LinkConstant would point to the robot model property; if
     * the inertia properties are specified in a custom frame, the value of that
     * property is not what we want, because RobCoGen-erated code need
     * properties in body coordinates. This class overrides the robot model
     * property with a custom value (the Var in the constructor) which must be
     * obtained after converting the inertia properties in body coordinates.
     */
    public static class OverriddenLinkConstant extends LinkConstant
    {
        new(AbstractLink l, IPField f, Var v) {
            super(l,f);
            actual = v;
            check();
        }
        override public modelvar(){ return actual }
        public final Var actual
    }

    new(Robot rob)
    {
        robot   = rob
        for(j : robot.joints) {
            for( ffield : nonZeroConstants( j.refFrame ) ) {
                val newc = new JointConstant(j, ffield)
                joints.add( newc )
                byVar .put( newc.modelvar, newc)
                byName.put( newc.name, newc)
                if( newc.isRotation ) {
                    rotations.add( newc )
                } else {
                    lengths.add( newc )
                }//
            }//
        }//

        for(l : robot.linksAndBase) {
            val lf_ip = l.linkFrameInertiaParams // make sure to use inertia properties in body coordinates
            for( IPfield : nonZeroConstants( lf_ip ) ) {
                val newc = new OverriddenLinkConstant(l, IPfield, utils.getVar(lf_ip, IPfield))
                inertias.add( newc )
                byVar .put( newc.modelvar, newc)
                byName.put( newc.name, newc)
            }
            for( fr : l.frames ) {
                for( c : nonZeroConstants( fr.transform ) ) {
                    val newc = new FrameConstant(fr, l, c)
                    frames.add( newc )
                    byVar .put( newc.modelvar, newc)
                    byName.put( newc.name, newc)
                    if( newc.isRotation ) {
                        rotations.add( newc )
                    } else {
                        lengths.add( newc )
                    }
                }//
            }//
        }//
    }//

    public final Robot robot
    public final Map<Var, ConstantInfo>   byVar  = new HashMap<Var, ConstantInfo>
    public final Map<String,ConstantInfo> byName = new HashMap<String,ConstantInfo>
    public final List<JointConstant> joints  = new ArrayList<JointConstant>()
    public final List<FrameConstant> frames  = new ArrayList<FrameConstant>()
    public final List<LinkConstant> inertias = new ArrayList<LinkConstant>()
    public final List<ConstantInfo> rotations= new ArrayList<ConstantInfo>()
    public final List<ConstantInfo> lengths  = new ArrayList<ConstantInfo>()

    private static extension Common utils = Common.getInstance()

    def static dispatch boolean isConst(FloatLiteral f) {
        return true
    }
    def static dispatch boolean isConst(Expr expr) {
        return isConst(expr.identifier)
    }
    def static dispatch boolean isConst(ParameterLiteral p) {
        return false
    }
    def static dispatch boolean isConst(PILiteral p) {
        return true
    }
    def static dispatch boolean isNonZeroConst(FloatLiteral f) {
        return (f.value != 0.0)
    }
    def static dispatch boolean isNonZeroConst(Expr expr) {
        return isNonZeroConst(expr.identifier)
    }
    def static dispatch boolean isNonZeroConst(ParameterLiteral p) {
        return false
    }
    def static dispatch boolean isNonZeroConst(PILiteral p) {
        return true
    }
    def static dispatch boolean isPIExpr(FloatLiteral expr) {
        return false
    }
    def static dispatch boolean isPIExpr(Expr expr) {
        return (expr.identifier instanceof PILiteral)
    }

    def nonZeroConstants(RotoTrasl ref)
    {
        val t = ref.translation
        val r = ref.rotation
        val ret = new ArrayList<JFField>()
        if (t.x.nonZeroConst) {
            ret.add(JFField.TX)
        }
        if (t.y.nonZeroConst) {
            ret.add(JFField.TY)
        }
        if (t.z.nonZeroConst) {
            ret.add(JFField.TZ)
        }
        if (r.x.nonZeroConst) {
            ret.add(JFField.RX)
        }
        if (r.y.nonZeroConst) {
            ret.add(JFField.RY)
        }
        if (r.z.nonZeroConst) {
            ret.add(JFField.RZ)
        }
        return ret
    }

    def nonZeroConstants(InertiaParams ip)
    {
        val ret = new ArrayList<IPField>()
        if(ip.mass.nonZeroConst) {
            ret.add(IPField.MASS)
        }
        if(ip.com.x.nonZeroConst) {
            ret.add(IPField.COMX)
        }
        if(ip.com.y.nonZeroConst) {
            ret.add(IPField.COMY)
        }
        if(ip.com.z.nonZeroConst) {
            ret.add(IPField.COMZ)
        }
        if(ip.ix.nonZeroConst) {
            ret.add(IPField.IX)
        }
        if(ip.ixy.nonZeroConst) {
            ret.add(IPField.IXY)
        }
        if(ip.ixz.nonZeroConst) {
            ret.add(IPField.IXZ)
        }
        if(ip.iy.nonZeroConst) {
            ret.add(IPField.IY)
        }
        if(ip.iyz.nonZeroConst) {
            ret.add(IPField.IYZ)
        }
        if(ip.iz.nonZeroConst) {
            ret.add(IPField.IZ)
        }
        return ret
    }

    def public static cname(AbstractLink l, IPField f) {
        return ip_cnames.get(f) + "_" + l.name
    }

    def public static  cname(Joint j, JFField f) {
        return jf_cnames.get(f) + "_" + j.name
    }

    def  public static cname(RefFrame fr, JFField f) {
        return jf_cnames.get(f) + "_" + fr.name
    }

    def  public static cname(String containerName, JFField f) {
        return jf_cnames.get(f) + "_" + containerName
    }

    def static name(Joint    j , JFField f) { return cname(j ,f).toString() }
    def static name(RefFrame fr, JFField f) { return cname(fr,f).toString() }
    def static name(AbstractLink l, IPField f) { return cname(l,f).toString() }


    private static val ip_cnames = #{
        IPField.MASS -> "m",
        IPField.COMX -> "comx",
        IPField.COMY -> "comy",
        IPField.COMZ -> "comz",
        IPField.IX   -> "ix",
        IPField.IXY  -> "ixy",
        IPField.IXZ  -> "ixz",
        IPField.IY   -> "iy",
        IPField.IYZ  -> "iyz",
        IPField.IZ   -> "iz"
    }
    private static val jf_cnames = #{
        JFField.TX -> "tx",
        JFField.TY -> "ty",
        JFField.TZ -> "tz",
        JFField.RX -> "rx",
        JFField.RY -> "ry",
        JFField.RZ -> "rz"
    }

}