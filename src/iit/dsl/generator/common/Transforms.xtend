package iit.dsl.generator.common

import iit.dsl.kinDsl.AbstractLink
import iit.dsl.generator.Common


class Transforms
{
    private static iit.dsl.coord.generator.Common ctdslCommon = iit.dsl.coord.generator.Common.getInstance()
    private static Common common = Common.getInstance()



    def static iit.dsl.coord.coordTransDsl.Transform getTransform(
        iit.dsl.coord.coordTransDsl.Model model, AbstractLink left, AbstractLink right)
    {
        return ctdslCommon.getTransform(model,
                common.getFrameName(left).toString(),
                common.getFrameName(right).toString())
    }

    def static l1_X_l2__defaultName(iit.dsl.coord.coordTransDsl.Model model,
        AbstractLink l1, AbstractLink l2)
    {
        return ctdslCommon.name(
            ctdslCommon.getTransform(model,
                common.getFrameName(l1).toString(),
                common.getFrameName(l2).toString())  )
    }

    def static l2_X_l1__defaultName(iit.dsl.coord.coordTransDsl.Model model,
        AbstractLink l1, AbstractLink l2)
    {
        return ctdslCommon.name(
            ctdslCommon.getTransform(model,
                common.getFrameName(l2).toString(),
                common.getFrameName(l1).toString())  )
    }

}