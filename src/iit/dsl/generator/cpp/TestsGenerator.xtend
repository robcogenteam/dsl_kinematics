package iit.dsl.generator.cpp

import iit.dsl.kinDsl.Robot

class TestsGenerator
{
    def public static cmdline_fd(Robot robot)
    '''
    #include <iit/robcogen/test/cmdline_fd.h>
    #include <«Names$Files.headersInstallPath(robot)»/«Names$Files::traitsHeader(robot)».h>

    /**
     * This program calls the generated implementation of Forward Dynamics, and
     * prints the result (i.e. the joint acceleration) on stdout.
     *
     * It requires all inputs to be given as command line arguments; there are «3*robot.joints.size»
     * arguments, for the position, velocity and joint force of each joint of
     * the robot. Group the arguments by type, not by joint.
     */
    int main(int argc, char** argv)
    {
        «IF robot.base.floating»
            iit::robcogen::test::cmdline_fd_fb< «Common.enclosingNamespacesQualifier(robot)»::Traits >(argc, argv);
        «ELSE»
            iit::robcogen::test::cmdline_fd< «Common.enclosingNamespacesQualifier(robot)»::Traits >(argc, argv);
        «ENDIF»
        return 0;
    }
    '''


    def public static cmdline_id(Robot robot)
    '''
    #include <iit/robcogen/test/cmdline_id.h>
    #include <«Names$Files.headersInstallPath(robot)»/«Names$Files::traitsHeader(robot)».h>

    «val ns = Common.enclosingNamespacesQualifier(robot)»

    /**
     * This program calls the generated implementation of Inverse Dynamics, and
     * prints the result (i.e. the joint forces) on stdout.
     *
     * It requires all inputs to be given as command line arguments; there are «3*robot.joints.size»
     * arguments, for the position, velocity and acceleration of each joint of
     * the robot. Group the arguments by type, not by joint.
     */
    int main(int argc, char** argv)
    {
        «IF robot.base.floating»
            iit::robcogen::test::cmdline_id_fb< «ns»::Traits >(argc, argv);
        «ELSE»
            iit::robcogen::test::cmdline_id< «ns»::Traits >(argc, argv);
        «ENDIF»
        return 0;
    }
    '''


    def public static cmdline_jsim(Robot robot)
    '''
    #include <iit/robcogen/test/cmdline_jsim.h>
    #include <«Names$Files.headersInstallPath(robot)»/«Names$Files::traitsHeader(robot)».h>

    /**
     * This program calls the generated implementation of the algorithm to calculate
     * the Joint Space Inertia Matrix, and prints it on stdout.
     *
     * It requires all inputs to be given as command line arguments; there are
     * «robot.joints.size» arguments, for the position status of each joint of
     * the robot.
     */
    int main(int argc, char** argv)
    {
        iit::robcogen::test::cmdline_jsim< «Common.enclosingNamespacesQualifier(robot)»::Traits >(argc, argv);
        return 0;
    }
    '''

    def public static cmdline_dyn_consistency(Robot robot)
    '''
    #include <iit/robcogen/test/dynamics_consistency.h>

    #include <«Names$Files.headersInstallPath(robot)»/«Names$Files$RBD.inertiaHeader(robot)».h>
    #include <«Names$Files.headersInstallPath(robot)»/«Names$Files.transformsHeader(robot)».h>
    #include <«Names$Files.headersInstallPath(robot)»/«Names$Files.traitsHeader(robot)».h>
    #include <«Names$Files.headersInstallPath(robot)»/«Names$Files.parametersHeader(robot)».h>
    #include <«Names$Files.headersInstallPath(robot)»/«Names$Files$RBD.massParametersHeader(robot)».h>

    using namespace «Common.enclosingNamespacesQualifier(robot)»;

    /**
     * This program calls the dynamics-consistency-test implemented in
     * iit::robcogen::test.
     *
     * No arguments are required, all the relevant joint-space quantities are
     * randomly generated.
     *
     * The test prints some output on stdout. All the numerical values should be
     * zero; if that is not the case, there is some inconsistency among the generated
     * dynamics algorithms.
     */
    int main(int argc, char** argv)
    {
        MotionTransforms xm;
        ForceTransforms  xf;
        InertiaProperties ip;

        iit::robcogen::test::consistencyTests<Traits>(ip, xm, xf);

        return 0;
    }
    '''


    def public static cmakefile(Robot robot)
    '''
    cmake_minimum_required(VERSION 2.8)
    project(robcogen-«robot.name»-cpptests)

    set(EIGEN_ROOT $ENV{EIGEN_ROOT}   CACHE PATH "Path to Eigen headers")
    set(CMAKE_CXX_FLAGS "-g -Wall -O3 -D EIGEN_NO_DEBUG -std=c++11")

    # Include directories
    include_directories(${EIGEN_ROOT})

    # Optional CppAD
    #
    set(CPPAD off CACHE BOOL "Enable tests of CppAD traits")
    if(CPPAD)
        set(CPPAD_ROOT $ENV{CPPAD_ROOT}   CACHE PATH "Path to CppAD headers")
        include_directories(${CPPAD_ROOT})
    endif(CPPAD)

    # The binaries
    add_executable(fd   «Names$Files$CmdLineTests.fd»)
    add_executable(id   «Names$Files$CmdLineTests.id»)
    add_executable(jsim «Names$Files$CmdLineTests.jsim»)
    add_executable(cons «Names$Files$CmdLineTests.dync»)

    «val lib = "iitgen" + robot.name.toLowerCase»
    target_link_libraries(fd   «lib»)
    target_link_libraries(id   «lib»)
    target_link_libraries(jsim «lib»)
    target_link_libraries(cons «lib»)
    '''

    private static extension iit.dsl.generator.Common = iit.dsl.generator.Common.getInstance()
}