package iit.dsl.generator.cpp

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.IFileSystemAccess
import java.util.ArrayList

import iit.dsl.kinDsl.Robot
import iit.dsl.generator.Jacobian
import iit.dsl.generator.cpp.dynamics.InverseDynamics
import iit.dsl.generator.cpp.dynamics.JointsSpaceInertia
import iit.dsl.generator.cpp.kinematics.Jacobians
import iit.dsl.generator.cpp.kinematics.Transforms
import iit.dsl.generator.cpp.dynamics.LinkInertias
import iit.dsl.generator.cpp.dynamics.ForwardDynamics
import iit.dsl.generator.cpp.config.IConfiguratorsGetter
import iit.dsl.generator.cpp.dynamics.InertiaParameters
import iit.dsl.generator.cpp.Misc

class Generator implements IGenerator
{
    new(IConfiguratorsGetter getter)
    {
        if(getter === null) {
            throw new RuntimeException("Cannot instantiate the C++ generator with a null IConfiguratorsGetter");
        }
        configGetter = getter

        Names::setConfigurators(
            configGetter.fileNamesConfigurator,
            configGetter.namespacesConfigurator,
            configGetter.classesAndTypesConfigurator)
    }

    private RobotHeaders  headers = new RobotHeaders()
    private Jacobians      jacobs = null
    private InverseDynamics invdyn= new InverseDynamics()
    private ForwardDynamics fordyn= new ForwardDynamics()
    private JointsSpaceInertia jsI= new JointsSpaceInertia()
    private LinkInertias inertias = null
    private InertiaParameters inertiaPar = null
    private Misc misc = new Misc()
    private Constants consts = new Constants()
    private MakefileGenerator mkg = new MakefileGenerator()

    private final IConfiguratorsGetter configGetter


	override void doGenerate(Resource resource, IFileSystemAccess fsa)
	{
    }

    def generateMiscellaneous(Robot robot, IFileSystemAccess fsa) {
        fsa.generateFile(Names$Files::miscHeader(robot) + ".h",
            misc.header(robot))
        fsa.generateFile(Names$Files::miscHeader(robot) + ".cpp",
            misc.source(robot))
    }

    def generateMakefiles(Robot robot, IFileSystemAccess fsa) {
        fsa.generateFile("CMakeLists.txt", mkg.CMakeFileBody(robot, configGetter.getMiscConfigurator().doConstantFolding()))
    }

    def generateCommons(Robot robot, IFileSystemAccess fsa) {
        fsa.generateFile(
            Names$Files::rbdTypesHeader(robot) + ".h",
            headers.types(robot)
        )
        fsa.generateFile(
            Names$Files::mainHeader(robot) + ".h",
            headers.main(robot)
        )
        fsa.generateFile(
            Names$Files::linkDataMapHeader(robot) + ".h",
            headers.linkDataMap(robot)
        )
        fsa.generateFile(
            Names$Files::jointDataMapHeader(robot) + ".h",
            headers.jointDataMap(robot)
        )
        fsa.generateFile(
            Names$Files::traitsHeader(robot) + ".h",
            headers.traits(robot)
        )

        if( ! configGetter.getMiscConfigurator().doConstantFolding() ) {
            fsa.generateFile(
                Names$Files::constantsHeader(robot) + ".h",
                consts.header(robot) )
        }

        generateMiscellaneous(robot, fsa)
    }



    def generateTransforms(
        Robot robot,
        IFileSystemAccess fsa,
        iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        val transforms = new Transforms(
            configGetter.getTransformsDSLGeneratorConfigurator(robot))
        transforms.generate(robot, fsa, transformsModel);
    }


    def generateInverseDynamicsStuff(Robot robot, IFileSystemAccess fsa, iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        fsa.generateFile(Names$Files$RBD::invDynHeader(robot) + ".h"  ,
            invdyn.mainHeader(robot, transformsModel) )
        fsa.generateFile(Names$Files$RBD::invDynSource(robot) + ".cpp",
            invdyn.inverseDynamicsImplementation(robot, transformsModel) )
    }
    def generateForwardDynamicsStuff(Robot robot, IFileSystemAccess fsa, iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        fsa.generateFile(Names$Files$RBD::fwdDynHeader(robot) + ".h"  , fordyn.headerContent(robot, transformsModel))
        fsa.generateFile(Names$Files$RBD::fwdDynHeader(robot) + ".cpp", fordyn.implementationFileContent(robot, transformsModel))
    }
    def generateInertiaMatrixStuff(Robot robot, IFileSystemAccess fsa, iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        fsa.generateFile(Names$Files$RBD::jsimHeader(robot)   + ".h",   jsI.inertiaMatrixHeader(robot))
        fsa.generateFile(Names$Files$RBD::jsimHeader(robot)   + ".cpp", jsI.inertiaMatrixSource(robot,transformsModel))
    }

    def generateLinkInertias(Robot robot, IFileSystemAccess fsa)
    {
        inertias = new LinkInertias(robot, configGetter.getMiscConfigurator().doConstantFolding())
        fsa.generateFile(Names$Files$RBD::inertiaHeader(robot) + ".h", inertias.header())
        fsa.generateFile(Names$Files$RBD::inertiaHeader(robot) + ".cpp", inertias.source())

        inertiaPar = new InertiaParameters(robot)
        fsa.generateFile(Names$Files$RBD::massParametersHeader(robot) + ".h", inertiaPar.headerFileContent())
    }

    def public generateJacobiansFiles(
        Robot robot,
        IFileSystemAccess fsa,
        iit.dsl.transspecs.transSpecs.DesiredTransforms desired,
        iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        val jacobians = new ArrayList<Jacobian>()
        if(desired !== null) {
            if(desired.jacobians !== null) {
                for(iit.dsl.transspecs.transSpecs.FramePair jSpec : desired.jacobians.getSpecs()) {
                    jacobians.add(new Jacobian(robot, jSpec))
                }
            }
        }
        jacobs = new Jacobians(robot, transformsModel, configGetter.jacobiansConfigurator)

        val String fname = Names$Files::jacobiansHeader(robot);
        fsa.generateFile( fname + ".h"  , jacobs.headerFile(jacobians) )
        fsa.generateFile( fname + ".cpp", jacobs.sourceFile(jacobians) )
    }

    // here we assume fsa to be already configured with the right path
    def public generateCmdLineTests(Robot robot, IFileSystemAccess fsa)
    {
        fsa.generateFile(Names$Files$CmdLineTests.fd  , TestsGenerator.cmdline_fd(robot))
        fsa.generateFile(Names$Files$CmdLineTests.id  , TestsGenerator.cmdline_id(robot))
        fsa.generateFile(Names$Files$CmdLineTests.jsim, TestsGenerator.cmdline_jsim(robot))
        fsa.generateFile(Names$Files$CmdLineTests.dync, TestsGenerator.cmdline_dyn_consistency(robot))
        fsa.generateFile("CMakeLists.txt", TestsGenerator.cmakefile(robot))
    }
}

