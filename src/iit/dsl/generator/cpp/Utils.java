package iit.dsl.generator.cpp;

public abstract class Utils {

    public static final int X_A = 0;
    public static final int Y_A = 1;
    public static final int Z_A = 2;
    public static final int X_L = 3;
    public static final int Y_L = 4;
    public static final int Z_L = 5;
}
