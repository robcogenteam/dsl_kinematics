package iit.dsl.generator.cpp

import iit.dsl.kinDsl.Robot

import iit.dsl.generator.cpp.Common
import iit.dsl.generator.common.Constants.ConstantInfo
import iit.dsl.generator.common.ConstantsAccess.Func


/**
 * Specialization of iit.dsl.generator.common.ConstantsAccess for C++ code
 * generators.
 */
class ConstantsAccess extends iit.dsl.generator.common.ConstantsAccess
{
    public new(Robot robot, boolean cFolding) {
        super( robot, cFolding, new CppConstantsExpressions() )
    }
}



/**
 * Generator of a header file defining C++ const variable for each
 * non-zero constant of the robot model.
 *
 * All the definitions are within the main robot namespace.
 */
class Constants
{
    def header(Robot robot) {
        val gen = new ConstantsAccess( robot, false )
        return
        '''
        #ifndef RCG_«robot.name.toUpperCase()»_«Names$Files::constantsHeader(robot).toUpperCase()»_H_
        #define RCG_«robot.name.toUpperCase()»_«Names$Files::constantsHeader(robot).toUpperCase()»_H_

        #include "«Names$Files::rbdTypesHeader(robot)».h"

        /**
         * \file
         * This file contains the definitions of all the non-zero numerical
         * constants of the robot model (i.e. the numbers appearing in the
         * .kindsl file).
         *
         * Varying these values (and recompiling) is a quick & dirty
         * way to vary the kinematics/dynamics model. For a much more
         * flexible way of exploring variations of the model, consider
         * using the parametrization feature of RobCoGen (see the wiki).
         *
         * Beware of inconsistencies when changing any of the inertia
         * properties.
         */

        «Common::enclosingNamespacesOpen(robot)»

        // Do not use 'constexpr' to allow for non-literal scalar types

        «gen.allDefinitions()»

        «Common::enclosingNamespacesClose(robot)»
        #endif
        '''
    }
}



class CppConstantsExpressions implements
                         iit.dsl.generator.common.ConstantsAccess.IConfigurator
{
    def public static cnameOfSineOf(ConstantInfo c) {
        return "sin_" + c.identifier
    }
    def public static cnameOfCosineOf(ConstantInfo c) {
        return "cos_" + c.identifier
    }
    /**
     * The identifier (ie variable name) used in the generated code for the
     * given constant
     */
    def public static identifier(ConstantInfo c) {
        return c.name
    }

    /*
     * In the generated C++, the constant values are defined as consts at
     * namespace scope. Therefore, the identifier associated to the constant
     * is the valid C++ expression that resolves to the value of the constant.
     */

    override valueExpression(ConstantInfo cinfo, Func f)
    {
        switch(f) {
            case NONE: {
                return cinfo.identifier
            }
            case SINE: {
                return cnameOfSineOf( cinfo )
            }
            case COSINE: {
                return cnameOfCosineOf( cinfo )
            }
        }
    }

    override definitionExpression(ConstantInfo c, Func f)
    {
        switch(f) {
            case NONE: {
                return expression(c.identifier, c.value.toString)
            }
            case SINE: {
                return expression(c.cnameOfSineOf, Names.Types.sinFName + "(" + c.identifier + ")" )
            }
            case COSINE: {
                return expression(c.cnameOfCosineOf, Names.Types.cosFName + "(" + c.identifier + ")" )
            }
        }
    }

    def private String expression(String name, String value) {
        return constTypeDef + " " + name + " = " + value + ";"
    }

    private static String constTypeDef = "const " + Names$Types.scalar
}


