package iit.dsl.generator.cpp

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.generator.common.Parameters
import iit.dsl.generator.common.Parameters.ParameterInfo
import iit.dsl.generator.cpp.dynamics.InertiaParameters


/**
 * Some facilities for generation of C++ code for parametric robot models.
 */
class CppParameters
{
    new(Parameters rob, Model transforms)
    {
        robotParams = rob
        transformsModel = transforms

        if( robotParams.lengths().size() > 0 ) {
            lengthsStructName = iit.dsl.coord.generator.cpp.Parameters.structName(
                Parameters.getLengthParamsGroup(transformsModel) )
        }
        if( robotParams.angles().size() > 0 ) {
            anglesStructName = iit.dsl.coord.generator.cpp.Parameters.structName(
                Parameters.getAngleParamsGroup(transformsModel) )
        }
    }

    /**
     * Returns the struct-type used as the container for the parameters.
     *
     * The caller might need to prepend the returned type with the qualifier
     * for the robot.
     */
    ///@{
    def public lengthsStructType() {
        return lengthsStructName
    }
    def public anglesStructType() {
        return anglesStructName
    }
    def public inertiasStructType() {
        return InertiaParameters.valuesStructName
    }
    ///@}

    /**
     * The field name that corresponds to the given parameter.
     * This is the name of the field of the struct that contains the parameters.
     * @return the string to be appended to a string in the form \c "<variable>."
     *    such that the resulting string is a valid piece of code that accesses
     *    the value of the given parameter
     * @throw a RuntimeException if the given parameter does not belong to the
     *    robot this instance was created with
     */
    def public structField(ParameterInfo p)
    {
        if( p.isKinematic() ) {
            val ct_param = Parameters.toTransformsDSLParameter(transformsModel, p)
            return iit.dsl.coord.generator.cpp.Parameters.structField(ct_param)
        }
        return InertiaParameters.structFieldName(p.emfObj)
    }


    public final Parameters robotParams
    public final iit.dsl.coord.coordTransDsl.Model transformsModel

    private String lengthsStructName = ""
    private String anglesStructName  = ""
}