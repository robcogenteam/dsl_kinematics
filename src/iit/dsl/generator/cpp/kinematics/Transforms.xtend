package iit.dsl.generator.cpp.kinematics

import org.eclipse.xtext.generator.IFileSystemAccess

import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.AbstractLink
import iit.dsl.generator.cpp.Names
import iit.dsl.coord.generator.cpp.ContainerClass

/**
 * Main generator of C++ code for coordinate transforms.
 *
 * This generator relies in turn on the code generator of the Transforms DSL
 * package.
 */
class Transforms
{

    def public static child_X_parent__mxName(AbstractLink parent, AbstractLink child) '''
        fr_«child.name»_X_fr_«parent.name»'''
    def public static parent_X_child__mxName(AbstractLink parent, AbstractLink child) '''
        fr_«parent.name»_X_fr_«child.name»'''


    /**
     */
    new(iit.dsl.coord.generator.cpp.IConfigurator configurator) {
       setTransformsDSLConfigurator(configurator)
    }

    /**
     * Sets the iit.dsl.coord.generator.cpp.IConfigurator object
     * that will be used by this generator
     */
    def public void setTransformsDSLConfigurator(
        iit.dsl.coord.generator.cpp.IConfigurator configurator)
    {
        if(configurator === null) {
            System::err.println("Transforms::setTransformsDSLConfigurator() : null configurator")
            //TODO log warning
            return
        }
        this.configurator = configurator
        transformsGenerator = new iit.dsl.coord.generator.cpp.ContainerClass(configurator)
    }

    def public generate(
        Robot robot,
        IFileSystemAccess fsa,
        iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        fsa.generateFile(
            Names$Files::parametersHeader(robot) + ".h",
            paramsFileGenerator.headerFileContent(
                transformsModel,
                #["\""+Names$Files.rbdTypesHeader(robot)+".h\""],
                configurator.enclosingNamespaces(transformsModel),
                configurator.scalarType )
        );
        transformsGenerator.setModel( transformsModel )
        fsa.generateFile(
            Names$Files::transformsHeader(robot) + ".h",
            transformsGenerator.headerFile()
        );
        fsa.generateFile(
            Names$Files::transformsSource(robot) + ".cpp",
            transformsGenerator.sourceFile()
        );
    }


    private iit.dsl.coord.generator.cpp.IConfigurator configurator = null
    private ContainerClass transformsGenerator = null
    private iit.dsl.coord.generator.cpp.Parameters paramsFileGenerator = new iit.dsl.coord.generator.cpp.Parameters()

}
