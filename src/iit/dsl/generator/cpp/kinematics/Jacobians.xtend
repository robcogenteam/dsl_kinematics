package iit.dsl.generator.cpp.kinematics

import java.util.List
import org.eclipse.xtend2.lib.StringConcatenation

import iit.dsl.kinDsl.Robot
import iit.dsl.generator.Jacobian
import iit.dsl.generator.cpp.Names
import iit.dsl.generator.cpp.Common
import iit.dsl.generator.cpp.VariableAccess
import iit.dsl.generator.cpp.ConstantsAccess
import iit.dsl.generator.maxima.Converter
import iit.dsl.generator.maxima.IConverterConfigurator

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.ParameterLiteral
import iit.dsl.coord.coordTransDsl.ParametersDeclaration
import iit.dsl.coord.coordTransDsl.Constant
import iit.dsl.coord.generator.cpp.IParametersAccess
import iit.dsl.coord.generator.cpp.Parameters
import iit.dsl.coord.generator.ProcessedMatrixInfo
import iit.dsl.coord.generator.TransformsInfo
import iit.dsl.coord.generator.Utilities
import iit.dsl.coord.generator.maxima.converter.Utils.TransformConversionInfo
import iit.dsl.coord.generator.MaximaDSLReplacements
import java.util.ArrayList
import iit.dsl.coord.generator.cpp.ContainerClass.ParametersStructMeta

class Jacobians
{
    public static interface IConfigurator
    {
        def boolean getConstantFolding()
        def IConverterConfigurator getMaximaConverterConfigurator()
    }

    def public static mainClassName() '''Jacobians'''
    def public static localClassName(Jacobian J) '''Type_«J.name»'''
    def public static updateParametersFunctionName() '''updateParameters'''
    /**
     * The name of the function that updates the Jacobian given a joint-state
     * variable.
     * The name must be consistent with the StateDependentMatrix defined in the
     * iit-rbd library
     */
    def public static updateFunctionName() { return "update" }

    def public containerClassQualifier(Robot rob)
        '''«Common::enclosingNamespacesQualifier(rob)»::«mainClassName()»'''

    public new(Robot robot, Model transforms, IConfigurator config)
    {
        this.robot = robot
        this.transformsModel = transforms
        this.paramGroups = transformsModel.paramGroups
        this.constsHelper    = new ConstantsAccess(robot, config.constantFolding)
        this.maximaConverter = new iit.dsl.generator.maxima.Converter(config.maximaConverterConfigurator)

        localBaseClassName = Names$Types::jacobianLocal
        jointStateTypeName = VariableAccess.jointStateType
        jointStateArgName  = VariableAccess.jointStateVar

        maximaReplSpecs = helper.makeMaximaDSLReplacements(
            transformsModel, new VariableAccess(robot), new ParamsAccess(),
            constsHelper.makeCTDSLConstantsAccess())

        val psCfg = new iit.dsl.coord.generator.cpp.ContainerClass.ParametersStructMeta.IConfigurator() {
            override scalarType()      { return Names$Types.scalar }
            override sinFunctionName() { return Names$Types.sinFName }
            override cosFunctionName() { return Names$Types.cosFName }
        }
        this.paramsStruct = new iit.dsl.coord.generator.cpp.ContainerClass.ParametersStructMeta(
            this.transformsModel, psCfg)
    }

    def headerFile(List<Jacobian> jacs)
    '''
        #ifndef «robot.name.toUpperCase()»_JACOBIANS_H_
        #define «robot.name.toUpperCase()»_JACOBIANS_H_

        #include <iit/rbd/TransformsBase.h>
        #include "«Names$Files::mainHeader(robot)».h"
        #include "«Names$Files::parametersHeader(robot)».h"
        #include "«Names$Files::transformsHeader(robot)».h" // to use the same 'Parameters' struct defined there
        «IF ! constsHelper.foldingEnabled»
            #include "«Names$Files::constantsHeader(robot)».h"
        «ENDIF»

        «Common::enclosingNamespacesOpen(robot)»

        template<int COLS, class M>
        class «localBaseClassName» : public «Names$Namespaces$Qualifiers::iit_rbd»::«Names$Types$IIT_RBD::jacobianBase(jointStateTypeName, "COLS", "M")»
        {};

        /**
         *
         */
        class «mainClassName()»
        {
            public:
                «localClassesDeclaration(jacs)»
            public:
                «mainClassName()»();
                void «updateParametersFunctionName»(«paramsArgsList(paramGroups)»);
            public:
                «FOR J : jacs»
                    «localClassName(J)» «J.name»;
                «ENDFOR»

            protected:
                «paramsStruct.typeName()» «member_paramsContainer»;

        };


        «Common::enclosingNamespacesClose(robot)»

        #endif
        '''
    private static final String member_paramsContainer = "params"

    static class JacInfo {
        public Jacobian J
        public TransformsInfo$Arguments info
        public new(Jacobian _J, TransformsInfo$Arguments _info) {
            this.J    = _J
            this.info = _info
        }
    }

    def sourceFile(List<Jacobian> jacs)
    {
        maximaConverter.load(robot, transformsModel)
        val jlist = new ArrayList<JacInfo>()
        for (J : jacs) {
            val transform= iit.dsl.coord.generator.Common.getInstance.getTransform(transformsModel, J.baseFrame.name, J.movingFrame.name)
            val info = new TransformsInfo$Arguments(transform)
            jlist.add( new JacInfo(J, info) )
        }
        val qualif = containerClassQualifier(robot)
        val code =
        '''
            #include "«Names$Files::jacobiansHeader(robot)».h"

            «qualif»::«mainClassName()»()
                «FOR J : jlist BEFORE ':' SEPARATOR ', '»
                    «J.J.name»(«IF J.info.isParametric»«member_paramsContainer»«ENDIF»)
                «ENDFOR»
            {}

            void «qualif»::«updateParametersFunctionName»(«paramsArgsList(paramGroups)»)
            {
                «FOR gr : paramGroups»
                    «member_paramsContainer».«ParametersStructMeta.ParamsGroup.field(gr)» = «formalParam(gr)»;
                «ENDFOR»
                «val field = ParametersStructMeta.AngleFunctions.field»
                «val update= ParametersStructMeta.AngleFunctions.updateFuncName»
                «member_paramsContainer».«field».«update»(«FOR p:paramsStruct.angles SEPARATOR','»«member_paramsContainer».«ParametersStructMeta.accessor(p)»«ENDFOR»);
            }

            «localClassesDefinitions(jlist)»
        '''

        maximaConverter.done()
        return code
    }


    def public localClassCTorArgs(TransformsInfo$Arguments info)
    {
        if(info.parametric) {
            return "const " + paramsStruct.typeName() +"& pp"
        }
        return ""
    }

    def private localClassesDeclaration( List<Jacobian> jacs)
    '''
        «FOR J : jacs»
            «val transform= iit.dsl.coord.generator.Common.getInstance.getTransform(transformsModel, J.baseFrame.name, J.movingFrame.name)»
            «val info = new TransformsInfo$Arguments(transform)»

            struct «localClassName(J)» : public «localBaseClassName»<«J.cols», «localClassName(J)»>
            {
                «localClassName(J)»(«localClassCTorArgs(info)»);
                const «localClassName(J)»& «updateFunctionName»(const «jointStateTypeName»&);
                «IF info.parametric»
                    protected:
                        const «paramsStruct.typeName()»& «member_paramsContainer»;
                «ENDIF»
            };

        «ENDFOR»
    '''

    def private localClassesDefinitions(List<JacInfo> jacs)
    {
        val code = new StringConcatenation
        for( J : jacs ) {
            val textInfo = maximaConverter.process(J.J)
            val cnvInfo  = new TransformConversionInfo(J.info, Utilities.MatrixType.HOMOGENEOUS, textInfo)

            code.append( constructorCode (J, textInfo, J.info.constants) )
            code.newLine() code.newLine()
            code.append( updateMethodCode(J.J, cnvInfo) )
            code.newLine() code.newLine()
        }
        return code
    }


    def private constructorCode(JacInfo J, ProcessedMatrixInfo info, List<Constant> constants)
    '''
        «val classname = localClassName(J.J)»
        «containerClassQualifier(J.J.robot)»::«classname»::«classname»(«localClassCTorArgs(J.info)»)
            «IF J.info.parametric» : «member_paramsContainer»(pp)«ENDIF»
        {
            «helper.matrixInitCode(info, constants, maximaReplSpecs)»
        }'''

    def private updateMethodCode(Jacobian J, TransformConversionInfo info)
    {
        val fqClass = containerClassQualifier(J.robot) + "::" + localClassName(J)
        val formalP = '''const «jointStateTypeName»& «jointStateArgName»'''
        return
        '''
        const «fqClass»& «fqClass»::«updateFunctionName»(«formalP»)
        {
            «helper.matrixUpdateCode(transformsModel, info, maximaReplSpecs)»
            return *this;
        }'''
    }


    def public static parameterValueAccessor(ParameterLiteral p, String function) {
        return member_paramsContainer + "." + ParametersStructMeta.accessor(p, function)
    }

    def private static formalParam(ParametersDeclaration gr) {
        return "_" + member(gr)
    }
    def private static member(ParametersDeclaration gr) {
        return gr.name
    }

    def private static paramsArgsList(List<ParametersDeclaration> groups)
        '''«FOR gr:groups SEPARATOR ", "»const «type(gr)»& «formalParam(gr)»«ENDFOR»'''


    def private static type(ParametersDeclaration gr) {
        return Parameters::structName(gr)
    }

    private static class ParamsAccess implements IParametersAccess
    {
        override valueExpression(Model model, ParameterLiteral p) {
            return Jacobians.parameterValueAccessor(p, "");
        }
        override valueExpression(Model model, ParameterLiteral p, String function) {
            return Jacobians.parameterValueAccessor(p, function);
        }
    }

    private final Robot robot
    private final iit.dsl.coord.coordTransDsl.Model  transformsModel
    private final iit.dsl.coord.generator.cpp.ContainerClass.ParametersStructMeta paramsStruct
    private final List<ParametersDeclaration> paramGroups
    private final ConstantsAccess constsHelper
    private final Converter maximaConverter
    //private boolean   use_cppusing = true
    private String    localBaseClassName = null
    private String    jointStateTypeName = null
    private String    jointStateArgName  = null

    private iit.dsl.coord.generator.MatrixGenCommon helper =
        new iit.dsl.coord.generator.MatrixGenCommon(
            new iit.dsl.coord.generator.cpp.LocalClasses.CommonGenConfig(
                Names$Types.scalar, Names$Types.sinFName, Names$Types.cosFName)
        )

    private MaximaDSLReplacements maximaReplSpecs = null
}


