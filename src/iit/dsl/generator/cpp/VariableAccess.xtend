package iit.dsl.generator.cpp

import iit.dsl.coord.generator.cpp.IVariablesAccess
import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.coordTransDsl.Variable
import iit.dsl.kinDsl.Robot


/**
 * Implementation of IVariablesAccess (@ Transforms-DSL) required by C++ generators
 * of the Kinematics-DSL
 */
class VariableAccess implements IVariablesAccess
{
    public static final String jointStateType = Names$Types::jointState
    public static final String jointStateVar  = "q"

    public new(Robot robot) {
        this.robot = robot
    }

    override valueExpression(Model model, Variable arg)
    {
        val joint = getJointFromVariableName(robot, arg.varname)
        if(joint === null){
            throw new RuntimeException("Could not find the joint corresponding to "+
                "variable " + arg.varname + ", for the robot " + robot.name)
        }
        return variablesStatusVector_varname(model)+"("+Common::jointIdentifier(joint)+")"
    }

    override valueExpression(Model model, Variable p, String function) {
        throw new UnsupportedOperationException("TODO: auto-generated method stub")
        // this one should get overridden by the TransformsDSL
    }

    override variablesStatusVector_type(Model model) {
        return jointStateType
    }

    override variablesStatusVector_varname(Model model) {
        return jointStateVar
    }

    private final Robot robot
    private extension iit.dsl.generator.Common = iit.dsl.generator.Common.getInstance()

}