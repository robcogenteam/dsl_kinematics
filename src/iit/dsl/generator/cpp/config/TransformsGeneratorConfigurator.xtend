package iit.dsl.generator.cpp.config

import iit.dsl.coord.coordTransDsl.Model

import iit.dsl.kinDsl.Robot
import iit.dsl.generator.cpp.Names
import iit.dsl.generator.cpp.VariableAccess
import iit.dsl.generator.cpp.ConstantsAccess
import iit.dsl.generator.cpp.Common

/**
 * The configurator for the C++ code generator in the Transforms DSL package.
 *
 * This implementation configures the C++ generator of the Transforms DSL such
 * that it generates code consistent with the rest of the robot-specific C++
 * code generated directly by this package.
 *
 * @see iit.dsl.coord.generator.cpp.DefaultConfigurator,
 *      iit.dsl.coord.generator.cpp.IConfigurator
 */
class TransformsGeneratorConfigurator extends iit.dsl.coord.generator.cpp.DefaultConfigurator
{

    new(Robot robot, IConfigurator$Paths pathConfig, boolean cFolding)
    {
        setRobot(robot)
        maximaConfigurator = new MaximaConverterConfigurator(pathConfig)
        this.constantFolding = cFolding
    }

    def public setRobot(Robot rob) {
        this.robot     = rob
        this.consAccess = new ConstantsAccess(this.robot, this.constantFolding)
        this.varsAccess = new VariableAccess(robot)
    }


    //
    // Directly from  iit.dsl.coord.generator.cpp.IConfigurator
    //

    override headerFileName(Model model) {
        Names$Files::transformsHeader(robot)
    }

    override paramsHeaderFileName(Model model) {
        Names$Files::parametersHeader(robot)
    }

    override includeDirectives(Model model)
        '''
        #include <iit/rbd/TransformsBase.h>
        #include "«Names$Files::mainHeader(robot)».h"
        «IF ! this.constantFolding»
            #include "«Names$Files::constantsHeader(robot)».h"
        «ENDIF»
        '''

    override enclosingNamespaces(Model model) {
        return Common.enclosingNamespacesNames(robot)
    }


    override className(iit.dsl.coord.generator.Utilities$MatrixType mxtype) {
        switch(mxtype) {
             case iit::dsl::coord::generator::Utilities$MatrixType::_6D:         Names$Types$Transforms::spatial_motion
             case iit::dsl::coord::generator::Utilities$MatrixType::_6D_FORCE:   Names$Types$Transforms::spatial_force
             case iit::dsl::coord::generator::Utilities$MatrixType::HOMOGENEOUS: Names$Types$Transforms::homogeneous
             default: throw(new RuntimeException("Unknown MatrixType: " + mxtype))
         }
    }

    override matrixBaseType(Model model, iit.dsl.coord.generator.Utilities$MatrixType mxtype) {
        switch(mxtype) {
             case iit::dsl::coord::generator::Utilities$MatrixType::_6D        : Names$Namespaces$Qualifiers::iit_rbd() + "::SpatialTransformBase"
             case iit::dsl::coord::generator::Utilities$MatrixType::_6D_FORCE  : Names$Namespaces$Qualifiers::iit_rbd() + "::SpatialTransformBase"
             case iit::dsl::coord::generator::Utilities$MatrixType::HOMOGENEOUS: Names$Namespaces$Qualifiers::iit_rbd() + "::HomogeneousTransformBase"
             default: throw(new RuntimeException("Unknown MatrixType: " + mxtype))
         }
    }

    override getMaximaConverterConfigurator() {
        return maximaConfigurator
    }

    override getConstantsValueExprGenerator(Model model) {
        return consAccess.makeCTDSLConstantsAccess()
    }

    override getVariablesValueExprGenerator(Model model) {
        return varsAccess
    }

    override scalarType() {
        return Names$Types.scalar
    }
    override sinFunctionName() {
        return Names$Types.sinFName
    }
    override cosFunctionName() {
        return Names$Types.cosFName
    }

    private Robot robot = null
    private ConstantsAccess consAccess = null
    private VariableAccess  varsAccess = null
    private MaximaConverterConfigurator maximaConfigurator = null
    private boolean constantFolding = false
}

