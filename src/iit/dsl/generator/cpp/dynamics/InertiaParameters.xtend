package iit.dsl.generator.cpp.dynamics

import iit.dsl.kinDsl.ParameterLiteral
import iit.dsl.kinDsl.Robot
import iit.dsl.generator.cpp.Common
import iit.dsl.generator.cpp.Names

import iit.dsl.generator.common.RobotInfoRegister
import iit.dsl.generator.common.Parameters
import iit.dsl.generator.common.Parameters.ParameterInfo

class InertiaParameters
{
    public static String valuesStructName = "RuntimeInertiaParams"

    def public static structFieldName(ParameterLiteral p) {
        return p.varname
    }
    def public static structFieldName(ParameterInfo p) {
        return p.name
    }

    new(Robot r) {
        robot  = r
        params = RobotInfoRegister.parameters(robot)
    }

    def public headerFileContent() '''
        #ifndef _«robot.name.toUpperCase»_RUNTIME_INERTIA_PARAMETERS_
        #define _«robot.name.toUpperCase»_RUNTIME_INERTIA_PARAMETERS_

        «Common::enclosingNamespacesOpen(robot)»

        /**
         * A container for the set of runtime inertia parameters of the robot
         * «robot.name».
         *
         * Inertia parameters are non-constant inertia-properties, symbolically
         * defined in the robot model.
         * As the value of the parameters must be resolved at runtime, we might refer
         * to them as "runtime parameters", "runtime dynamics parameters",
         * "runtime inertia parameters", etc.
         *
         * Unfortunately, the literature commonly refers to the inertia-properties
         * as "inertia parameters". Do not confuse them. In RobCoGen, the parameters
         * are the non-constant values of the properties.
         */
        «valuesStructDefinition»

        «Common::enclosingNamespacesClose(robot)»
        #endif
    '''



    def protected valuesStructDefinition() '''
        struct «valuesStructName» {
            «FOR p : params.inertias»
                «Names$Types.scalar» «structFieldName(p)»;
            «ENDFOR»
            «valuesStructName»() {
                defaults();
            }
            void defaults() {
                «IF params.inertias.size > 0»
                    // Delete this comment and the #error only after replacing the zeros with appropriate initial values
                    // This is a temporary hack, waiting for a mechanism in RobCoGen to specify default values in the robot model
                    #error "You MUST change this code to provide a valid default value for your inertia parameters"
                «ENDIF»
                «FOR p : params.inertias»
                    «structFieldName(p)» = 0.0; // TODO change the value!!
                «ENDFOR»
            }
        };
    '''


    private Robot robot = null
    private Parameters params = null;
}