package iit.dsl.generator.cpp.dynamics

import java.util.List
import java.util.Map
import java.util.HashMap

import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.Link
import iit.dsl.kinDsl.AbstractLink
import iit.dsl.generator.cpp.Names
import iit.dsl.generator.cpp.RobotHeaders
import iit.dsl.generator.cpp.Common
import iit.dsl.generator.common.Transforms


class InverseDynamics {

    def static className(Robot r) '''InverseDynamics'''
    def static forceGetterName(AbstractLink l) '''getForce_«l.name»'''
    def static velocityGetterName(AbstractLink l) '''getVelocity_«l.name»'''
    def static accelGetterName(AbstractLink l) '''getAcceleration_«l.name»'''

    def mainHeader(
        Robot robot,
        iit.dsl.coord.coordTransDsl.Model transformsModel)
    '''
        «loadInfo(robot, transformsModel)»
        #ifndef RCG_«robot.name.toUpperCase()»_«Names$Files$RBD::invDynHeader(robot).toUpperCase()»_H_
        #define RCG_«robot.name.toUpperCase()»_«Names$Files$RBD::invDynHeader(robot).toUpperCase()»_H_

        #include <iit/rbd/rbd.h>
        #include <iit/rbd/InertiaMatrix.h>
        #include <iit/rbd/utils.h>

        #include "«Names$Files::mainHeader(robot)».h"
        #include "«Names$Files$RBD::inertiaHeader(robot)».h"
        #include "«Names$Files::transformsHeader(robot)».h"
        #include "«Names$Files::linkDataMapHeader(robot)».h"

        «Common::enclosingNamespacesOpen(robot)»

        /**
         * The Inverse Dynamics routine for the robot «robot.name».
         *
         * In addition to the full Newton-Euler algorithm, specialized versions to compute
         * only certain terms are provided.
         * The parameters common to most of the methods are the joint status vector \c q, the
         * joint velocity vector \c qd and the acceleration vector \c qdd.
         *
         * Additional overloaded methods are provided without the \c q parameter. These
         * methods use the current configuration of the robot; they are provided for the
         * sake of efficiency, in case the motion transforms of the robot have already
         * been updated elsewhere with the most recent configuration (eg by a call to
         * setJointStatus()), so that it is useless to compute them again.
         *
         * Whenever present, the external forces parameter is a set of external
         * wrenches acting on the robot links. Each wrench must be expressed in
         * the reference frame of the link it is excerted on.
         */
        class «className(robot)» {
        public:
            typedef «RobotHeaders::linkDataMap_type()»<Force> «extF_t»;

            /**
             * Default constructor
             * \param in the inertia properties of the links
             * \param tr the container of all the spatial motion transforms of
             *     the robot «robot.name», which will be used by this instance
             *     to compute inverse-dynamics.
             */
            «className(robot)»(«LinkInertias::className(robot)»& in, «Names$Types$Transforms::spatial_motion»& tr);

            «val params ='''«fParam_qd», «fParam_qdd», «fParam_fext» = zeroExtForces'''»
            «IF floatingBase»
                /** \name Inverse dynamics
                 * The full algorithm for the inverse dynamics of this robot.
                 *
                 * All the spatial vectors in the parameters are expressed in base coordinates,
                 * besides the external forces: each force must be expressed in the reference
                 * frame of the link it is acting on.
                 * \param[out] «name_jointsForce» the joint force vector required to achieve the desired accelerations
                 * \param[out] «name_baseAccel» the spatial acceleration of the robot base
                 * \param[in] g the gravity acceleration, as a spatial vector;
                 *              gravity implicitly specifies the orientation of the base in space
                 * \param[in] «robot.base.velocity» the spatial velocity of the base
                 * \param[in] q the joint position vector
                 * \param[in] qd the joint velocity vector
                 * \param[in] qdd the desired joint acceleration vector
                 * \param[in] fext the external forces acting on the links; this parameters
                 *            defaults to zero
                 */ ///@{
                void id(
                    «fParam_tau», «fParam_basea»,
                    «fParam_gravity», «fParam_basev»,
                    «fParam_q», «fParam_qd», «fParam_qdd»,
                    «fParam_fext» = zeroExtForces);
                void id(
                    «fParam_tau», «fParam_basea»,
                    «fParam_gravity», «fParam_basev»,
                    «fParam_qd», «fParam_qdd»,
                    «fParam_fext» = zeroExtForces);
                ///@}
                /** \name Inverse dynamics, fully actuated base
                 * The inverse dynamics algorithm for the floating base robot,
                 * in the assumption of a fully actuated base.
                 *
                 * All the spatial vectors in the parameters are expressed in base coordinates,
                 * besides the external forces: each force must be expressed in the reference
                 * frame of the link it is acting on.
                 * \param[out] «name_baseWrench» the spatial force to be applied to the robot base to achieve
                 *             the desired accelerations
                 * \param[out] «name_jointsForce» the joint force vector required to achieve the desired accelerations
                 * \param[in] g the gravity acceleration, as a spatial vector;
                 *              gravity implicitly specifies the orientation of the base in space
                 * \param[in] «robot.base.velocity» the spatial velocity of the base
                 * \param[in] «name_baseAccel» the desired spatial acceleration of the robot base
                 * \param[in] q the joint position vector
                 * \param[in] qd the joint velocity vector
                 * \param[in] qdd the desired joint acceleration vector
                 * \param[in] fext the external forces acting on the links; this parameters
                 *            defaults to zero
                 */ ///@{
                void id_fully_actuated(
                    «fParam_basef», «fParam_tau»,
                    «fParam_gravity», «fParam_basev», «fParam_basea_const»,
                    «fParam_q», «params»);
                void id_fully_actuated(
                    «fParam_basef», «fParam_tau»,
                    «fParam_gravity», «fParam_basev», «fParam_basea_const»,
                    «params»);
                ///@}
                /** \name Gravity terms, fully actuated base
                 */
                ///@{
                void G_terms_fully_actuated(
                    «fParam_basef», «fParam_tau»,
                    «fParam_gravity», «fParam_q»);
                void G_terms_fully_actuated(
                    «fParam_basef», «fParam_tau»,
                    «fParam_gravity»);
                ///@}
                /** \name Centrifugal and Coriolis terms, fully actuated base
                 *
                 * These functions take only velocity inputs, that is, they assume
                 * a zero spatial acceleration of the base (in addition to zero acceleration
                 * at the actuated joints).
                 * Note that this is NOT the same as imposing zero acceleration
                 * at the virtual 6-dof-floting-base joint, which would result, in general,
                 * in a non-zero spatial acceleration of the base, due to velocity
                 * product terms.
                 */
                ///@{
                void C_terms_fully_actuated(
                    «fParam_basef», «fParam_tau»,
                    «fParam_basev», «fParam_q», «fParam_qd»);
                void C_terms_fully_actuated(
                    «fParam_basef», «fParam_tau»,
                    «fParam_basev», «fParam_qd»);
                ///@}
            «ELSE»
                /** \name Inverse dynamics
                 * The full Newton-Euler algorithm for the inverse dynamics of this robot.
                 *
                 * \param[out] «name_jointsForce» the joint force vector required to achieve the desired accelerations
                 * \param[in] q the joint position vector
                 * \param[in] qd the joint velocity vector
                 * \param[in] qdd the desired joint acceleration vector
                 * \param[in] fext the external forces acting on the links; this parameters
                 *            defaults to zero
                 */
                ///@{
                void id(
                    «fParam_tau»,
                    «fParam_q», «fParam_qd», «fParam_qdd»,
                    «fParam_fext» = zeroExtForces);
                void id(
                    «fParam_tau»,
                    «fParam_qd», «fParam_qdd»,
                    «fParam_fext» = zeroExtForces);
                ///@}

                /** \name Gravity terms
                 * The joint forces (linear or rotational) required to compensate
                 * for the effect of gravity, in a specific configuration.
                 */
                ///@{
                void G_terms(«fParam_tau», «fParam_q»);
                void G_terms(«fParam_tau»);
                ///@}

                /** \name Centrifugal and Coriolis terms
                 * The forces (linear or rotational) acting on the joints due to centrifugal and
                 * Coriolis effects, for a specific configuration.
                 */
                ///@{
                void C_terms(«fParam_tau», «fParam_q», «fParam_qd»);
                void C_terms(«fParam_tau», «fParam_qd»);
                ///@}
            «ENDIF»
            /** Updates all the kinematics transforms used by the inverse dynamics routine. */
            void setJointStatus(«fParam_q») const;

        public:
            /** \name Getters
             * These functions return various spatial quantities used internally
             * by the inverse dynamics routines, like the spatial acceleration
             * of the links.
             *
             * The getters can be useful to retrieve the additional data that is not
             * returned explicitly by the inverse dynamics routines even though it
             * is computed. For example, after a call to the inverse dynamics,
             * the spatial velocity of all the links has been determined and
             * can be accessed.
             *
             * However, beware that certain routines might not use some of the
             * spatial quantities, which therefore would retain their last value
             * without being updated nor reset (for example, the spatial velocity
             * of the links is unaffected by the computation of the gravity terms).
             */
            ///@{
            «IF floatingBase»
                const Force& «forceGetterName(robot.base)»() const { return «robot.base.force»; }
            «ENDIF»
            «FOR l : robot.links»
                const Velocity& «velocityGetterName(l)»() const { return «l.velocity»; }
                const Acceleration& «accelGetterName(l)»() const { return «l.acceleration»; }
                const Force& «forceGetterName(l)»() const { return «l.force»; }
            «ENDFOR»
            ///@}
        protected:
            «IF floatingBase»
                void secondPass_fullyActuated(«fParam_tau»);
            «ELSE»
                void firstPass(«fParam_qd», «fParam_qdd», «fParam_fext»);
                void secondPass(«fParam_tau»);
            «ENDIF»

        private:
            «LinkInertias::className(robot)»* «linkInertiasMember»;
            «Names$Types$Transforms::spatial_motion»* «motionTransformsMember»;
        private:
            Matrix66 vcross; // support variable
            «FOR l : robot.links»
                // Link '«l.name»' :
                const InertiaMatrix& «l.inertia»;
                Velocity      «l.velocity»;
                Acceleration  «l.acceleration»;
                Force         «l.force»;
            «ENDFOR»

            «IF floatingBase»
                // The robot base
                const InertiaMatrix& «robot.base.inertia»;
                InertiaMatrix «robot.base.inertiaC»;
                Force         «robot.base.force»;
                // The composite inertia tensors
                «FOR l : robot.links»
                    «IF l.childrenList.children.empty»
                        const InertiaMatrix& «l.inertiaC»;
                    «ELSE»
                        InertiaMatrix «l.inertiaC»;
                    «ENDIF»
                «ENDFOR»
            «ENDIF»

        private:
            static const «extF_t» zeroExtForces;
        };

        inline void «className(robot)»::setJointStatus(«fParam_q») const
        {
            «setJointStatusCode()»
        }

        «IF floatingBase»
            inline void «className(robot)»::id(
                «fParam_tau», «fParam_basea»,
                «fParam_gravity», «fParam_basev»,
                «fParam_q», «fParam_qd», «fParam_qdd»,
                «fParam_fext»)
            {
                setJointStatus(q);
                id(«name_jointsForce», «robot.base.acceleration», g, «robot.base.velocity»,
                   qd, qdd, fext);
            }

            inline void «className(robot)»::G_terms_fully_actuated(
                «fParam_basef», «fParam_tau»,
                «fParam_gravity», «fParam_q»)
            {
                setJointStatus(q);
                G_terms_fully_actuated(«name_baseWrench», «name_jointsForce», g);
            }

            inline void «className(robot)»::C_terms_fully_actuated(
                «fParam_basef», «fParam_tau»,
                «fParam_basev», «fParam_q», «fParam_qd»)
            {
                setJointStatus(q);
                C_terms_fully_actuated(«name_baseWrench», «name_jointsForce», «robot.base.velocity», qd);
            }

            inline void «className(robot)»::id_fully_actuated(
                    «fParam_basef», «fParam_tau»,
                    «fParam_gravity», «fParam_basev», «fParam_basea_const»,
                    «fParam_q», «fParam_qd», «fParam_qdd», «fParam_fext»)
            {
                setJointStatus(q);
                id_fully_actuated(«name_baseWrench», «name_jointsForce», g, «robot.base.velocity»,
                    «name_baseAccel», qd, qdd, fext);
            }
        «ELSE»
            inline void «className(robot)»::G_terms(«fParam_tau», «fParam_q»)
            {
                setJointStatus(q);
                G_terms(«name_jointsForce»);
            }

            inline void «className(robot)»::C_terms(«fParam_tau», «fParam_q», «fParam_qd»)
            {
                setJointStatus(q);
                C_terms(«name_jointsForce», qd);
            }

            inline void «className(robot)»::id(
                «fParam_tau»,
                «fParam_q», «fParam_qd», «fParam_qdd»,
                «fParam_fext»)
            {
                setJointStatus(q);
                id(«name_jointsForce», qd, qdd, fext);
            }
        «ENDIF»

        «Common::enclosingNamespacesClose(robot)»

        #endif
        '''

    def inverseDynamicsImplementation(
        Robot robot,
        iit.dsl.coord.coordTransDsl.Model transformsModel)
    '''
            #include <iit/rbd/robcogen_commons.h>

            #include "«Names$Files$RBD::invDynHeader(robot)».h"
            #include "«Names$Files$RBD::inertiaHeader(robot)».h"
            #ifndef EIGEN_NO_DEBUG
                #include <iostream>
            #endif
            «val nsqualifier = Common.enclosingNamespacesQualifier(robot)»
            using namespace std;
            using namespace «Names$Namespaces$Qualifiers::iit_rbd»;
            using namespace «nsqualifier»;

            // Initialization of static-const data
            const «nsqualifier»::«className(robot)»::«extF_t»
            «nsqualifier»::«className(robot)»::zeroExtForces(Force::Zero());

            «nsqualifier»::«className(robot)»::«className(robot)»(«LinkInertias::className(robot)»& inertia, «Names$Types$Transforms::spatial_motion»& transforms) :
                «linkInertiasMember»( & inertia ),
                «motionTransformsMember»( & transforms ),
                «FOR l : robot.links SEPARATOR ','»
                    «l.inertia»(«linkInertiasMember»->«LinkInertias::tensorGetterName(l)»() )
                «ENDFOR»
                «IF floatingBase»,
                    «robot.base.inertia»( «linkInertiasMember»->«LinkInertias::tensorGetterName(robot.base)»() ),
                    «FOR l : robot.chainEndLinks SEPARATOR ','»
                        «l.inertiaC»(«l.inertia»)
                     «ENDFOR»
                «ENDIF»
            {
            #ifndef EIGEN_NO_DEBUG
                std::cout << "Robot «robot.name», «className(robot)»::«className(robot)»()" << std::endl;
                std::cout << "Compiled with Eigen debug active" << std::endl;
            #endif
                «FOR l : robot.links»
                    «l.velocity».setZero();
                «ENDFOR»

                vcross.setZero();
            }

            «IF floatingBase»
                «methodsDefinition_floatingBase()»
            «ELSE»
                «methodsDefinition_fixedBase()»
            «ENDIF»
            '''

    private static CharSequence name_baseWrench  = '''baseWrench'''
    private static CharSequence name_jointsForce = '''jForces'''
    private static CharSequence name_baseAccel   = '''baseAccel'''
    def private fParam_q()   '''const «jState»& q'''
    def private fParam_qd()  '''const «jState»& qd'''
    def private fParam_qdd() '''const «jState»& qdd'''
    def private fParam_tau() '''«jState»& «name_jointsForce»'''
    def private fParam_fext()'''const «extF_t»& fext'''
    def private fParam_basea()   '''Acceleration& «robot.base.acceleration»'''
    def private fParam_basea_const()   '''const Acceleration& «name_baseAccel»'''
    def private fParam_basev()   '''const Velocity& «robot.base.velocity»'''
    def private fParam_basef()   '''Force& «name_baseWrench»'''
    def private fParam_gravity() '''const Acceleration& g'''



    def private methodsDefinition_fixedBase() '''
        «val nsqualifier = Common.enclosingNamespacesQualifier(robot)»
        void «nsqualifier»::«className(robot)»::id(
            «fParam_tau»,
            «fParam_qd», «fParam_qdd»,
            «fParam_fext»)
        {
            firstPass(qd, qdd, fext);
            secondPass(«name_jointsForce»);
        }

        void «nsqualifier»::«className(robot)»::G_terms(«fParam_tau»)
        {
            «fixedBase_pass1_gravityTerms()»

            secondPass(«name_jointsForce»);
        }

        void «nsqualifier»::«className(robot)»::C_terms(«fParam_tau», «fParam_qd»)
        {
            «fixedBase_pass1_Cterms()»

            secondPass(«name_jointsForce»);
        }


        void «nsqualifier»::«className(robot)»::firstPass(«fParam_qd», «fParam_qdd», «fParam_fext»)
        {
            «fixedBase_pass1()»
        }

        void «nsqualifier»::«className(robot)»::secondPass(«fParam_tau»)
        {
            «fixedBase_pass2()»
        }
    '''

    def private methodsDefinition_floatingBase() '''
        «val nsqualifier = Common.enclosingNamespacesQualifier(robot)»
        void «nsqualifier»::«className(robot)»::id(
            «fParam_tau», «fParam_basea»,
            «fParam_gravity», «fParam_basev»,
            «fParam_qd», «fParam_qdd»,
            «fParam_fext»)
        {
            «IF floatingBase»
                «robot.base.inertiaC» = «robot.base.inertia»;
                «FOR l : robot.links»
                    «IF ! l.childrenList.children.empty»
                        «l.inertiaC» = «l.inertia»;
                    «ENDIF»
                «ENDFOR»
            «ENDIF»

            «floatingBase_pass1()»

            // Add the external forces:
            «addFextCode()»

            «floatingBase_pass2»

            «floatingBase_pass3»

            «robot.base.acceleration» += g;
        }


        void «nsqualifier»::«className(robot)»::G_terms_fully_actuated(
            «fParam_basef», «fParam_tau»,
            «fParam_gravity»)
        {
            const Acceleration& «robot.base.acceleration» = -g;

            «fixedBase_pass1_gravityTerms()»

            «robot.base.force» = «robot.base.inertia» * «robot.base.acceleration»;

            secondPass_fullyActuated(«name_jointsForce»);

            «name_baseWrench» = «robot.base.force»;
        }

        void «nsqualifier»::«className(robot)»::C_terms_fully_actuated(
            «fParam_basef», «fParam_tau»,
            «fParam_basev», «fParam_qd»)
        {
            «fixedBase_pass1_Cterms()»

            «robot.base.force» = vxIv(«robot.base.velocity», «robot.base.inertia»);

            secondPass_fullyActuated(«name_jointsForce»);

            «name_baseWrench» = «robot.base.force»;
        }

        void «nsqualifier»::«className(robot)»::id_fully_actuated(
                «fParam_basef», «fParam_tau»,
                «fParam_gravity», «fParam_basev», «fParam_basea_const»,
                «fParam_qd», «fParam_qdd», «fParam_fext»)
        {
            Acceleration «robot.base.acceleration» = «name_baseAccel» -g;

            «fixedBase_pass1()»

            // The base
            «robot.base.force» = «robot.base.inertia» * «robot.base.acceleration» + vxIv(«robot.base.velocity», «robot.base.inertia») - fext[«Common::linkIdentifier(robot.base)»];

            secondPass_fullyActuated(«name_jointsForce»);

            «name_baseWrench» = «robot.base.force»;
        }


        void «nsqualifier»::«className(robot)»::secondPass_fullyActuated(«fParam_tau»)
        {
            «fixedBase_pass2()»
        }
    '''


    def private fixedBase_pass1_gravityTerms() '''
        «FOR Link l : sortedLinks»
            // Link '«l.name»'
            «val parent  = l.parent»
            «val acceler = l.acceleration»
            «val child_X_parent = transformsMap.get(l)»
            «IF parent.equals(robot.base) && !floatingBase»
                «acceler» = («child_X_parent»).col(«rbd_ns»::LZ) * «Common.enclosingNamespacesQualifier(robot)»::g;
            «ELSE»
                «acceler» = («child_X_parent») * «parent.acceleration»;
            «ENDIF»
            «l.force» = «l.inertia» * «acceler»;
        «ENDFOR»
     '''

    def private fixedBase_pass1_Cterms() '''
        «FOR Link l : sortedLinks»
            «val parent   = l.parent»
            «val joint    = l.connectingJoint»
            «val velocity = l.velocity»
            «val acceler  = l.acceleration»
            «val child_X_parent = transformsMap.get(l)»
            «val jid = Common::jointIdentifier(joint)»
            «val subspaceIdx = iit::dsl::generator::cpp::Common::spatialVectIndex(joint)»
            // Link '«l.name»'
            «IF floatingBase»
                «velocity» = ((«child_X_parent») * «parent.velocity»);
                «velocity»(«subspaceIdx») += qd(«jid»);
                motionCrossProductMx<«Names$Types.scalar»>(«velocity», vcross);
                «IF parent.equals(robot.base)»
                    «acceler» = (vcross.col(«subspaceIdx») * qd(«jid»));
                «ELSE»
                    «acceler» = («child_X_parent») * «parent.acceleration» + vcross.col(«subspaceIdx») * qd(«jid»);
                «ENDIF»
                «l.force» = «l.inertia» * «acceler» + vxIv(«velocity», «l.inertia»);
            «ELSE»
                «IF parent.equals(robot.base)»
                    «velocity»(«subspaceIdx») = qd(«jid»);   // «velocity» = vJ, for the first link of a fixed base robot

                    «IF joint.prismatic»
                        // The first joint is prismatic, no centripetal terms.
                        «l.force».setZero();
                    «ELSE»
                        «l.force» = vxIv(qd(«jid»), «l.inertia»);
                    «ENDIF»
                «ELSE»
                    «velocity» = ((«child_X_parent») * «parent.velocity»);
                    «velocity»(«subspaceIdx») += qd(«jid»);

                    motionCrossProductMx<«Names$Types.scalar»>(«velocity», vcross);

                    «IF parent.parent.equals(robot.base)»
                        «acceler» = (vcross.col(«subspaceIdx») * qd(«jid»));
                    «ELSE»
                        «acceler» = («child_X_parent») * «parent.acceleration» + vcross.col(«subspaceIdx») * qd(«jid»);
                    «ENDIF»

                    «l.force» = «l.inertia» * «acceler» + vxIv(«velocity», «l.inertia»);
                «ENDIF»
            «ENDIF»

        «ENDFOR»
    '''

    def private fixedBase_pass1() '''
        «FOR Link l : sortedLinks»
            «val parent   = l.parent»
            «val myJoint  = l.connectingJoint»
            «val velocity = l.velocity»
            «val acceler  = l.acceleration»
            «val child_X_parent = transformsMap.get(l)»
            «val jid = Common::jointIdentifier(myJoint)»
            «val subspaceIdx = iit::dsl::generator::cpp::Common::spatialVectIndex(myJoint)»
            // First pass, link '«l.name»'
            «IF parent.equals(robot.base) && !floatingBase»
                «acceler» = («child_X_parent»).col(«rbd_ns»::LZ) * «Common.enclosingNamespacesQualifier(robot)»::g;
                «acceler»(«subspaceIdx») += qdd(«jid»);
                «velocity»(«subspaceIdx») = qd(«jid»);   // «velocity» = vJ, for the first link of a fixed base robot

                «IF myJoint.prismatic»
                    // The first joint is prismatic, no centripetal terms.
                    «l.force» = «l.inertia» * «acceler» - fext[«Common::linkIdentifier(l)»];
                «ELSE»
                    «l.force» = «l.inertia» * «acceler» + vxIv(qd(«jid»), «l.inertia»)  - fext[«Common::linkIdentifier(l)»];
                «ENDIF»
            «ELSE»
                «velocity» = ((«child_X_parent») * «parent.velocity»);
                «velocity»(«subspaceIdx») += qd(«jid»);

                motionCrossProductMx<«Names$Types.scalar»>(«velocity», vcross);

                «acceler» = («child_X_parent») * «parent.acceleration» + vcross.col(«subspaceIdx») * qd(«jid»);
                «acceler»(«subspaceIdx») += qdd(«jid»);

                «l.force» = «l.inertia» * «acceler» + vxIv(«velocity», «l.inertia») - fext[«Common::linkIdentifier(l)»];
            «ENDIF»

        «ENDFOR»
    '''



    def private fixedBase_pass2() '''
        «FOR l : sortedLinks.reverseView»
            // Link '«l.name»'
            «val parent = l.parent»
            «val joint  = l.connectingJoint»
            «name_jointsForce»(«Common::jointIdentifier(joint)») = «l.force»(«iit::dsl::generator::cpp::Common::spatialVectIndex(joint)»);
            «IF ( ! parent.equals(robot.base)) || floatingBase»
                «val child_X_parent = transformsMap.get(l)»
                «parent.force» += «child_X_parent».transpose() * «l.force»;
            «ENDIF»
        «ENDFOR»
    '''

    def private floatingBase_pass1() '''
        «FOR Link l : sortedLinks»
            «val parent   = l.parent»
            «val myJoint  = l.connectingJoint»
            «val velocity = l.velocity»
            «val acceler  = l.acceleration»
            «val child_X_parent = transformsMap.get(l)»
            «val jid = Common::jointIdentifier(myJoint)»
            «val subspaceIdx = iit::dsl::generator::cpp::Common::spatialVectIndex(myJoint)»
            // First pass, link '«l.name»'
            «velocity» = ((«child_X_parent») * «parent.velocity»);
            «velocity»(«subspaceIdx») += qd(«jid»);

            motionCrossProductMx<«Names$Types.scalar»>(«velocity», vcross);

            «IF parent.equals(robot.base)/* parent is the floating base */»
                «acceler» = (vcross.col(«subspaceIdx») * qd(«jid»));
            «ELSE»
                «acceler» = («child_X_parent») * «parent.acceleration» + vcross.col(«subspaceIdx») * qd(«jid»);
            «ENDIF»
            «acceler»(«subspaceIdx») += qdd(«jid»);

            «l.force» = «l.inertia» * «acceler» + vxIv(«velocity», «l.inertia»);

        «ENDFOR»
        // The force exerted on the floating base by the links
        «val base = robot.base»
        «base.force» = vxIv(«base.velocity», «base.inertia»);

    '''

    def private floatingBase_pass2() '''
        InertiaMatrix Ic_spare;
        «FOR l : sortedLinks.reverseView()»
            «val parent = l.parent»
            «val child_X_parent = transformsMap.get(l)»
            «rbd_ns»::transformInertia<«Names$Types.scalar»>(«l.inertiaC», («child_X_parent»).transpose(), Ic_spare);
            «parent.inertiaC» += Ic_spare;
            «parent.force» = «parent.force» + («child_X_parent»).transpose() * «l.force»;

        «ENDFOR»
    '''

    def private floatingBase_pass3() '''
        // The base acceleration due to the force due to the movement of the links
        «robot.base.acceleration» = - «robot.base.inertiaC».inverse() * «robot.base.force»;

        «FOR l : sortedLinks»
            «val parent = l.parent»
            «val joint  = l.connectingJoint»
            «val child_X_parent = transformsMap.get(l)»
            «val idx = iit::dsl::generator::cpp::Common::spatialVectIndex(joint)»
            «l.acceleration» = «child_X_parent» * «parent.acceleration»;
            «name_jointsForce»(«Common::jointIdentifier(joint)») = («l.inertiaC».row(«idx») * «l.acceleration» + «l.force»(«idx»));

        «ENDFOR»
    '''

    def private setJointStatusCode() '''
        «FOR l : sortedLinks»
            («transformsMap.get(l)»)(q);
        «ENDFOR»
    '''

    def private addFextCode() '''
        «IF floatingBase»
            «robot.base.force» -= fext[«Common::linkIdentifier(robot.base)»];
        «ENDIF»
        «FOR l : sortedLinks»
            «l.force» -= fext[«Common::linkIdentifier(l)»];
        «ENDFOR»
    '''

    def private linkInertiasMember() '''inertiaProps'''
    def private motionTransformsMember() '''xm'''
    def private Xmotion(iit.dsl.coord.coordTransDsl.Transform t)
        '''«motionTransformsMember»->«iit::dsl::coord::generator::cpp::ContainerClass::memberName(t)»'''

    def private void loadInfo(Robot rob, iit.dsl.coord.coordTransDsl.Model transforms)
    {
        if(robot == rob) return;
        robot = rob
        dofs = robot.DOFs
        jointDOFs = robot.jointDOFs
        floatingBase = robot.base.floating
        transformsModel = transforms
        sortedLinks = robot.links.sortBy(link | getID(link))
        for(l : robot.links) {
            transformsMap.put(l, Xmotion(Transforms::getTransform(transformsModel, l, l.parent)))
        }
    }

    private Robot robot
    private int dofs
    private int jointDOFs
    private boolean floatingBase
    private iit.dsl.coord.coordTransDsl.Model transformsModel
    private List<Link> sortedLinks
    private Map<Link, CharSequence> transformsMap = new HashMap<Link, CharSequence>();

    private extension iit.dsl.generator.Common common = new iit.dsl.generator.Common()
    private extension VariableNames = new VariableNames

    private static String rbd_ns = Names$Namespaces$Qualifiers::iit_rbd
    private static String jState = Names$Types::jointState
    private static String extF_t = Names$Types::extForces
}