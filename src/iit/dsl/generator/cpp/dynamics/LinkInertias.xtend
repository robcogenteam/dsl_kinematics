package iit.dsl.generator.cpp.dynamics

import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.AbstractLink
import iit.dsl.kinDsl.InertiaParams
import iit.dsl.kinDsl.Var
import iit.dsl.kinDsl.ParameterLiteral
import iit.dsl.kinDsl.MultExpr
import iit.dsl.kinDsl.DivExpr
import iit.dsl.kinDsl.PlainExpr

import iit.dsl.generator.Common.IPField
import iit.dsl.generator.common.Parameters
import iit.dsl.generator.common.Constants.LinkConstant
import iit.dsl.generator.cpp.Names
import iit.dsl.generator.cpp.ConstantsAccess


class LinkInertias
{
    def public static className(Robot r) '''InertiaProperties'''
    def public static updateParamsMethodName() '''updateParameters'''
    def public static tensorGetterName(AbstractLink l) '''getTensor_«l.name»'''
    def public static massGetterName(AbstractLink l)   '''getMass_«l.name»'''
    def public static comGetterName(AbstractLink l)   '''getCOM_«l.name»'''

    new(Robot robot) {
        this(robot, false)
    }

    new(Robot robot, boolean cFolding)
    {
        this.robot = robot
        this.constantsHelper = new ConstantsAccess(robot, cFolding)
    }

    def header() '''
        #ifndef RCG_«robot.name.toUpperCase()»_«Names$Files$RBD::inertiaHeader(robot).toUpperCase()»_H_
        #define RCG_«robot.name.toUpperCase()»_«Names$Files$RBD::inertiaHeader(robot).toUpperCase()»_H_

        #include <iit/rbd/rbd.h>
        #include <iit/rbd/InertiaMatrix.h>
        #include <iit/rbd/utils.h>

        #include "«Names$Files::mainHeader(robot)».h"
        «IF ! constantsHelper.foldingEnabled()»
            #include "«Names$Files::constantsHeader(robot)».h"
        «ENDIF»
        #include "«Names$Files$RBD::massParametersHeader(robot)».h"

        «iit::dsl::generator::cpp::Common::enclosingNamespacesOpen(robot)»

        «val links = getRelevantLinks(robot)»
        class «className(robot)» {
            public:
                «className(robot)»();
                ~«className(robot)»();
                «FOR l : links»
                    const InertiaMatrix& «tensorGetterName(l)»() const;
                «ENDFOR»
                «FOR l : links»
                    «Names$Types.scalar» «massGetterName(l)»() const;
                «ENDFOR»
                «FOR l : links»
                    const «Names$Types::vector3d»& «comGetterName(l)»() const;
                «ENDFOR»
                «Names$Types.scalar» getTotalMass() const;


                /*!
                 * Fresh values for the runtime parameters of the robot «robot.name»,
                 * causing the update of the inertia properties modeled by this
                 * instance.
                 */
                void «updateParamsMethodName()»(const «params_t»&);

            private:
                «params_t» «memberName_params»;

                «FOR l : links»
                    InertiaMatrix «memberName_tensor(l)»;
                «ENDFOR»
                «FOR l : links»
                    «Names$Types::vector3d» «memberName_com(l)»;
                «ENDFOR»
        };


        inline «className(robot)»::~«className(robot)»() {}

        «FOR l : links»
            inline const InertiaMatrix& «className(robot)»::«tensorGetterName(l)»() const {
                return this->«memberName_tensor(l)»;
            }
        «ENDFOR»
        «FOR l : links»
            inline «Names$Types.scalar» «className(robot)»::«massGetterName(l)»() const {
                return this->«memberName_tensor(l)».getMass();
            }
        «ENDFOR»
        «FOR l : links»
            inline const «Names$Types::vector3d»& «className(robot)»::«comGetterName(l)»() const {
                return this->«memberName_com(l)»;
            }
        «ENDFOR»

        inline «Names$Types.scalar» «className(robot)»::getTotalMass() const {
            return «FOR l : links SEPARATOR " + "»«value(l.inertiaParams.mass,l,IPField.MASS)»«ENDFOR»;
        }

        «iit::dsl::generator::cpp::Common::enclosingNamespacesClose(robot)»

        #endif
    '''

    def source() '''
        «val nsqualifier    = iit.dsl.generator.cpp.Common.enclosingNamespacesQualifier(robot)»
        «val classqualifier = nsqualifier + "::" + className(robot)»
        «val links = getRelevantLinks(robot)»
        #include "«Names$Files$RBD::inertiaHeader(robot)».h"

        using namespace std;
        using namespace «Names$Namespaces$Qualifiers::iit_rbd»;

        «classqualifier»::«className(robot)»()
        {
            «FOR l : links»
                «val params = getLinkFrameInertiaParams(l)»
                «memberName_com(l)» = «comRHS(l, params)»;
                «memberName_tensor(l)».fill(
                    «value(params.mass, l, IPField.MASS)»,
                    «memberName_com(l)»,
                    «buildTensorExpr(l, params)» );

            «ENDFOR»
        }


        void «classqualifier»::«updateParamsMethodName()»(const «params_t»& fresh)
        {
            this-> «memberName_params» = fresh;
            «FOR l : links»
                «updateRuntimeParamsCode(l)»
            «ENDFOR»
        }
    '''

    def private updateRuntimeParamsCode(AbstractLink link) '''
        «val inertia = getLinkFrameInertiaParams(link)»
        «val parametricFlags = Parameters::whoIsParametric(inertia)»
        «IF( parametricFlags.all )»
            «memberName_com(link)» = «comRHS(link, inertia)»;
                «memberName_tensor(link)».fill(
                    «value(inertia.mass, link, IPField.MASS)»,
                    «memberName_com(link)»,
                    «buildTensorExpr(link, inertia)»);
        «ELSE»
            «IF( parametricFlags.mass)»
                «memberName_tensor(link)».changeMass(«value(inertia.mass, link, IPField.MASS)»);
            «ENDIF»
            «IF( parametricFlags.com)»
                «memberName_com(link)» = «comRHS(link, inertia)»;
                «memberName_tensor(link)».changeCOM(«memberName_com(link)»);
            «ENDIF»
            «IF( parametricFlags.tensor)»
                «memberName_tensor(link)».changeRotationalInertia(
                    «buildTensorExpr(link, inertia)»);
            «ENDIF»
        «ENDIF»
    '''

    def private comRHS(AbstractLink link, InertiaParams ip)
    {
        val x = value(ip.com.x, link, IPField.COMX)
        val y = value(ip.com.y, link, IPField.COMY)
        val z = value(ip.com.z, link, IPField.COMZ)
        return '''«Names$Types::vector3d»(«x»,«y»,«z»)'''
    }

    def private buildTensorExpr(AbstractLink link, InertiaParams ip)
    {
        val ix  = value(ip.ix , link, IPField.IX)
        val ixy = value(ip.ixy, link, IPField.IXY)
        val ixz = value(ip.ixz, link, IPField.IXZ)
        val iy  = value(ip.iy , link, IPField.IY)
        val iyz = value(ip.iyz, link, IPField.IYZ)
        val iz  = value(ip.iz , link, IPField.IZ)
        return '''Utils::buildInertiaTensor<«Names$Types.scalar»>(«ix»,«iy»,«iz»,«ixy»,«ixz»,«iyz»)'''
    }

    def private value(Var v, AbstractLink l, IPField f)
    {
        if( Parameters::isParameter(v) )
        {
            return pvalue( v )
        }
        // The Var is either a constant value or a simple
        // expression of a constant. Generate then the expression
        // that would resolve to such a constant
        return constantsHelper.valueExpr( new LinkConstant(l,f) )
    }

    def private dispatch CharSequence pvalue(Var v) {}

    def private dispatch CharSequence pvalue(ParameterLiteral p)
        '''«IF p.minus»(-«ENDIF»«memberName_params».«InertiaParameters.structFieldName(p)»«IF p.minus»)«ENDIF»'''

    def private dispatch CharSequence pvalue(PlainExpr expr) {
        return pvalue(expr.identifier)
    }

    def private dispatch CharSequence pvalue(MultExpr expr)
        '''(«expr.mult» * «pvalue(expr.identifier)»)'''

    def private dispatch CharSequence pvalue(DivExpr expr)
        '''(«pvalue(expr.identifier)»/«expr.div»)'''


    def private memberName_tensor(AbstractLink l) '''tensor_«l.name»'''
    def private memberName_com(AbstractLink l) '''com_«l.name»'''


    def private getRelevantLinks(Robot robot) {
        if(robot.base.floating) {
            return robot.abstractLinks //include the base body
        } else {
            return robot.links
        }
    }

    private extension iit.dsl.generator.Common common = iit.dsl.generator.Common.getInstance()
    private final Robot robot
    private final ConstantsAccess constantsHelper

    private static final String params_t = InertiaParameters.valuesStructName
    private static final String memberName_params = "params"
}
