package iit.dsl.generator

import iit.dsl.kinDsl.Robot
import iit.dsl.generator.matlab.RoysModel
import iit.dsl.kinDsl.KinDslPackage
import iit.dsl.generator.common.RobotInfoRegister

class TestsGenerator
{
   public interface IConfigurator
   {
        def public String octaveTestsSrcRoot();

        def public String cppTestsSrcDir(Robot robot);
        def public String genOctaveDir(Robot robot);
        def public String genModelsDir(Robot robot);

        def public String octaveStartupScriptName();
    }

    public new(IConfigurator configurator)
    {
        this.config= configurator
    }

    def public getConfigurator() { return this.config }

    def public octaveStartup(Robot robot)
    {
        val parametric = RobotInfoRegister.parameters(robot).isParametric
        val out_kk = "me.kk"
        val out_pp = "me.pp"
        var initArgs = out_kk
        if(parametric) {
            initArgs += ", " + out_pp
        }
    '''
    %% Possibly fix the paths according to your configuration

    [rc spv2] = system('locate -b spatial_v2'); % does not work in every OS
    if( rc != 0 )
        error('Could not find spatial_v2 on the file system. Aborting');
    end
    spv2 = spv2(1:end-1); % get rid of garbage

    addpath(genpath( spv2 ) );                % The root of 'spatial_v2'
    addpath('«config.genOctaveDir(robot)»');  % The robot-specific Octave generated code
    addpath('«config.genModelsDir(robot)»');  % The generated model in Roy's format (also Octave)

    % Add to the path the Octave-tests root
    %
    if( ! exist('octave-tests', 'dir') )
        error('Please make a symbolic link to the "octave-tests/" subfolder of the RobCoGen root');
    end
    addpath('octave-tests/src/');
    addpath('octave-tests/src/cpp');

    %% These paths were automatically generated! Check them!
    exe_id = '«config.cppTestsSrcDir(robot)»/build/id';
    exe_fd = '«config.cppTestsSrcDir(robot)»/build/fd';
    exe_im = '«config.cppTestsSrcDir(robot)»/build/jsim';

    «out_kk» = modelConstants();
    «IF parametric»
        me.pp = «iit.dsl.generator.matlab.Parameters.initFunctionName»();
    «ENDIF»
    me.ip = inertiaProperties(«initArgs»);
    me.xm = initMotionTransforms(«initArgs»);
    me.xf = initForceTransforms(«initArgs»);

    roy.model = «RoysModel.functionName(robot)»(«initArgs»);
    '''
    }

    def public testExecShellScript(Robot robot)
    {
        var String suffix = ""
        if(robot.base.eClass.equals( KinDslPackage.eINSTANCE.floatingRobotBase)) {
            suffix = "_fb"
        }
        return
        '''
        #!/usr/bin/octave -qf

        ## This Octave program demonstrates how to use the test functions shipped with
        ## RobCoGen, to test the generated code (both Octave and C++). Please refer to
        ## '<robcogen root>/octave-tests/readme.md' for more information.

        ## Set up the workspace first:
        run «config.octaveStartupScriptName»


        printf('\nTest of the generated Octave code for the Joint Space Inertia Matrix\n');
        [me.res roy.res] = test_jsim«suffix»(roy.model, me);

        printf('\nTest of the generated Octave code for Inverse Dynamics\n');
        [me.res roy.res] = test_id«suffix»(roy.model, me);

        printf('\nTest of the generated Octave code for Forward Dynamics\n');
        [me.res roy.res] = test_fd«suffix»(roy.model, me);


        printf('\nTest of the generated C++ code for the Joint Space Inertia Matrix\n');
        [me.res roy.res] = testcpp_jsim«suffix»(roy.model, exe_im);

        printf('\nTest of the generated C++ code for Inverse Dynamics\n');
        [me.res roy.res] = testcpp_id«suffix»(roy.model, exe_id);

        printf('\nTest of the generated C++ code for Forward Dynamics\n');
        [me.res roy.res] = testcpp_fd«suffix»(roy.model, exe_fd);
        '''
    }

    private IConfigurator config
}