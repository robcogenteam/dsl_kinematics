package iit.dsl.generator.matlab

import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.Var
import iit.dsl.kinDsl.InertiaParams
import iit.dsl.kinDsl.AbstractLink

import iit.dsl.generator.Common
import iit.dsl.generator.Common.IPField
import iit.dsl.generator.Utilities

import java.util.List
import java.util.ArrayList
import iit.dsl.generator.common.RobotInfoRegister

class InertiaProperties
{
    public static class StructFields
    {
        def public static link(AbstractLink l) { return "lf_" + l.name }
        def public static mass  ()  { return "mass" }
        def public static com   ()  { return "com" }
        def public static tensor()  { return "tensor" }
        def public static stensor() { return "tensor6D" }
    }

    public static CharSequence functionName = '''inertiaProperties'''
    public static CharSequence fieldName_spatialInertia = '''tensor6D'''

    def public static fieldName(AbstractLink l) {
        return "lf_" + l.name
    }

    /**
     * Matlab code that evaluates to the 3x3 tensor of the given link
     */
    def public static CharSequence tensor(AbstractLink l, InertiaParams ip, ValueExprBuilder builder)
    {
        val ix  = builder.valueExpr(ip.ix , l, IPField.IX)
        val ixy = builder.valueExpr(ip.ixy, l, IPField.IXY)
        val ixz = builder.valueExpr(ip.ixz, l, IPField.IXZ)
        val iy  = builder.valueExpr(ip.iy , l, IPField.IY)
        val iyz = builder.valueExpr(ip.iyz, l, IPField.IYZ)
        val iz  = builder.valueExpr(ip.iz , l, IPField.IZ)

        return '''
        [[ «ix»  ,«TAB»-(«ixy»),«TAB»-(«ixz»)];
        [-(«ixy»),«TAB»  «iy»  ,«TAB»-(«iyz»)];
        [-(«ixz»),«TAB»-(«iyz»),«TAB»  «iz»]]'''
    }

    /**
     * Matlab code that evaluates to the 3x1 vector with the COM position, as
     * contained in the given inertia properties.
     */
    def public static com(AbstractLink l, InertiaParams ip, ValueExprBuilder builder)
    {
        val x = builder.valueExpr(ip.com.x, l, IPField.COMX)
        val y = builder.valueExpr(ip.com.y, l, IPField.COMY)
        val z = builder.valueExpr(ip.com.z, l, IPField.COMZ)
        return '''[«x»; «y»; «z»]'''
    }

    /**
     * Matlab code that evaluates to the mass of the given link
     */
    def public static mass(AbstractLink l, InertiaParams ip, ValueExprBuilder builder)
    {
        return builder.valueExpr(ip.mass, l, IPField.MASS)
    }

    /**
     * Main code generator method.
     *
     * \return the body of the function that would
     * create a structure with all the inertia properties of the robot.
     */
    def public functionBody(Robot robot, boolean cFolding)
    {
        val parametric = RobotInfoRegister.parameters(robot).inertiaIsParametric
        constantFolding = cFolding
        exprBuilder = new ValueExprBuilder(robot, fargConsts, fargParams, constantFolding)
        var String args = ""
        if( !constantFolding ) {
            // include the argument for the model parameters regardless whether
            // the robot is parametric or not, as the argument can be ignored
            args = fargConsts + ", " + fargParams
        } else if( parametric ) {
            args = fargParams
        }

        if( parametric || !constantFolding )
        {
            return functionBody_linkFrameOnly(robot, args)
        } else {
            // the generation of the function with inertia properties in multiple
            // frames only works when the model is not parametric and when
            // constant-folding is enabled, otherwise we would not be able to
            // perform here in the code generator the required math to roto-
            // translate the inertia properties
            return functionBody_multipleFrames(robot)
        }
    }


    def private functionBody_linkFrameOnly(Robot robot, String args) '''
        %% Inertia properties expressed in the default link-frame, for each
        %% link of the robot «robot.name».
        %%
        %% OUT = «functionName.toString.toUpperCase»(«fargConsts.toUpperCase», «fargParams.toUpperCase»)
        %%
        %% OUT is a structure with a field for each link of the robot, each
        %% field being in turn a struct with the inertia properties.
        %%
        %% Depending on the original robot model and on the code generator
        %% configuration, this function may take up to two arguments:
        %%
        %% «fargConsts.toUpperCase» is a struct with all the constant properties
        %% of the robot model; it is required if constant-folding was disabled
        %% during code generation. Otherwise, the constant numerical properties
        %% of the model appear explicitly as float literals.
        %%
        %% «fargParams.toUpperCase» is a struct with the current value of the
        %% possibly varying model properties, i.e. the model parameters. This
        %% argument is required if the original robot model is parametric. If
        %% not, ignore the argument.
        %%
        %% This file has been automatically generated by RobCoGen.

        function out = «functionName»(«args»)

        «FOR l : robot.abstractLinks»
            «val ip = l.linkFrameInertiaParams»
            «val struct = "out." + StructFields.link(l)»
            «struct».«StructFields.mass»   = «mass(l, ip, this.exprBuilder)»;
            «struct».«StructFields.com»    = «com (l, ip, this.exprBuilder)»;
            «struct».«StructFields.tensor» = ...
                «tensor(l, ip, this.exprBuilder)»;

            com = «struct».«StructFields.com»;
            block = [  0,    -com(3),  com(2);
                     com(3),  0,      -com(1);
                    -com(2),  com(1),  0 ] * «struct».mass;
            «struct».«StructFields.stensor» = [«struct».«StructFields.tensor», block; block', «struct».«StructFields.mass»*eye(3)];


        «ENDFOR»
    '''

    /**
     * Generates Matlab code that creates several structs with the inertia parameters
     * of the links of the given robot.
     * Each struct has three members, the inertia tensor, the COM location (3d vector) and the mass.
     * This code contains the values corresponding to the inertia parameters as contained
     * in the robot model, as well as parameters expressed in the default frame of each link
     * and in a frame centered in the COM, aligned with the link-frame. Therefore, three
     * structures are created for each rigid body of the given robot.
     */
    def private functionBody_multipleFrames(Robot robot) {
        val List<InertiaParams> userFrame = new ArrayList<InertiaParams>()
        val List<InertiaParams> linkFrame = new ArrayList<InertiaParams>()
        val List<InertiaParams> comFrame  = new ArrayList<InertiaParams>()
        allInertiaParams(robot, userFrame, linkFrame, comFrame)
        val userFrameIt = userFrame.iterator()
        val linkFrameIt = linkFrame.iterator()
        val comFrameIt  = comFrame.iterator()
        val allLinks = robot.abstractLinks
        val outVar = "out"

        return'''
        function «outVar» = «functionName»()

        % Inertia parameters as written in the .kindsl model file
        «FOR l : allLinks»
            «val tmp = userFrameIt.next»
            «val struct = outVar + "." + l.name»
            «struct».mass = «value(tmp.mass)»;
            «struct».tensor = ...
                «tensor(tmp)»;
            «struct».com = «com(tmp)»;

        «ENDFOR»

        % Now the same inertia parameters expressed in the link frame (may be equal or not to
        %  the previous ones, depending on the model file)
        «FOR l : allLinks»
            «val tmp = linkFrameIt.next»
            «val struct = outVar + "." + fieldName(l)»
            «struct».mass = «value(tmp.mass)»;
            «struct».tensor = ...
                «tensor(tmp)»;
            «struct».com = «com(tmp)»;
            com = «struct».com;
            block = [  0,    -com(3),  com(2);
                     com(3),  0,      -com(1);
                    -com(2),  com(1),  0 ] * «struct».mass;
            «struct».«fieldName_spatialInertia» = [«struct».tensor, block; block', «struct».mass*eye(3)];

        «ENDFOR»

        % Same inertial properties expressed in a frame with origin in the COM of the link
        %  oriented as the default link-frame (the COM coordinates in such a frame should
        %  always be [0,0,0] ).
        «FOR l : allLinks»
            «val tmp = comFrameIt.next»
            «val struct = outVar + ".com_" + l.name»
            «struct».mass = «value(tmp.mass)»;
            «struct».tensor = ...
                «tensor(tmp)»;
            «struct».com = «com(tmp)»;

        «ENDFOR»
    '''}

    /*
     * Fills three lists with the inertia parameters of all the links, expressed in
     * different frames.
     * First list: parameters as written in the robot model
     * Second list: parameters expressed in the default link-frame
     * Third list: parameters expressed in a frame with origin in the COM, aligned
     *             as the default link-frame
     * All the lists are ordered as robot.abstractLinks
     */
    def private allInertiaParams(Robot rob,
        List<InertiaParams> userFrame,
        List<InertiaParams> linkFrame,
        List<InertiaParams> comFrame)
    {
        // Start with the robot base
        var InertiaParams inLinkFrame = null
        var InertiaParams inComFrame  = null
        for(l : rob.abstractLinks) {
            inLinkFrame = l.linkFrameInertiaParams
            inComFrame  = Utilities::rototranslate(inLinkFrame, inLinkFrame.com.x.asFloat,
                inLinkFrame.com.y.asFloat, inLinkFrame.com.z.asFloat, 0,0,0, false)

            userFrame.add(l.inertiaParams)
            linkFrame.add(inLinkFrame)
            comFrame.add(inComFrame)
        }
    }

    /*
     * The float literal for the given property.
     * Assumes the property is a constant.
     */
    def private value(Var ip) {
        return ip.asFloat
    }

    /*
     * Matlab code that evaluates to the 3x3 tensor of the given inertia properties.
     */
    def private CharSequence tensor(InertiaParams params) '''
        [[  «value(params.ix)»,«TAB»-(«value(params.ixy)»),«TAB»-(«value(params.ixz)»)];
         [-(«value(params.ixy)»),«TAB»  «value(params.iy)» ,«TAB»-(«value(params.iyz)»)];
         [-(«value(params.ixz)»),«TAB»-(«value(params.iyz)»),«TAB»  «value(params.iz)»]]'''

    /*
     * Matlab code that evaluates to the 3x1 vector with the COM position, as
     * contained in the given inertia properties.
     */
    def private com(InertiaParams params) '''
         [«value(params.com.x)»; «value(params.com.y)»; «value(params.com.z)»]'''



    private boolean constantFolding = true
    private ValueExprBuilder exprBuilder = null
    private extension Common = Common::getInstance()

    private static String fargParams = "params"
    private static String fargConsts = "consts"
    private static String TAB = "\t"

}