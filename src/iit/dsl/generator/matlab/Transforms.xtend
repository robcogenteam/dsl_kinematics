package iit.dsl.generator.matlab


import iit.dsl.kinDsl.Robot
import iit.dsl.generator.matlab.config.IConfigurator
import iit.dsl.coord.generator.Utilities

class Transforms
{
    private iit.dsl.coord.generator.matlab.GenerationCore matlabGen = null
    private iit.dsl.coord.coordTransDsl.Model transModel = null

    new(Robot robot,
        iit.dsl.coord.coordTransDsl.Model transformsModel,
        IConfigurator configurator)
    {
        transModel = transformsModel
        matlabGen  =  new iit.dsl.coord.generator.matlab.GenerationCore(configurator)
        matlabGen.setModel( transModel )
    }

	def public homogeneous_init_fileContent(Robot robot) {
	    matlabGen.init_fileContent(Utilities$MatrixType::HOMOGENEOUS)
	}

	def public homogeneous_update_fileContent(Robot robot) {
        matlabGen.update_fileContent(Utilities$MatrixType::HOMOGENEOUS)
    }

    def public motion6D_init_fileContent(Robot robot) {
        matlabGen.init_fileContent(Utilities$MatrixType::_6D)
    }

    def public motion6D_update_fileContent(Robot robot) {
        matlabGen.update_fileContent(Utilities$MatrixType::_6D)
    }

    def public force6D_init_fileContent(Robot robot) {
        matlabGen.init_fileContent(Utilities$MatrixType::_6D_FORCE)
    }

    def public force6D_update_fileContent(Robot robot) {
        matlabGen.update_fileContent(Utilities$MatrixType::_6D_FORCE)
    }
}