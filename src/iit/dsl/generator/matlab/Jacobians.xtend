package iit.dsl.generator.matlab

import iit.dsl.kinDsl.Robot
import iit.dsl.generator.Jacobian
import iit.dsl.generator.matlab.config.IConfigurator
import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.generator.matlab.StructBasedParsAccess

import org.eclipse.xtend2.lib.StringConcatenation

import java.util.List
import java.util.ArrayList
import iit.dsl.coord.generator.TransformsInfo
import iit.dsl.coord.generator.maxima.converter.Utils.TransformConversionInfo
import iit.dsl.coord.generator.Utilities

class Jacobians
{

    public static String updateFunctionName = "updateJacobians"
    public static String initFunctionName   = "initJacobians"

    public new(Robot robot, Model transforms, IConfigurator config)
    {
        this.robot       = robot
        this.transforms  = transforms
        this.configurator= config
        this.maximaConverter = new iit.dsl.generator.maxima.Converter(
                                configurator.kindslMaximaConverterConfigurator )
        this.maxdslAccess    = new iit.dsl.maxdsl.utils.DSLAccessor()

        maximaReplSpecs = helper.makeMaximaDSLReplacements(
            transforms, config.getVariablesValueExprGenerator(transforms),
            new StructBasedParsAccess("params"),
            config.getConstantsValueExprGenerator(transforms) )
    }

    def public getJacobiansLocalVarName() { return jacobiansStructName }


    /**
     * The complete content of the file with Matlab function to initialize the Jacobians
     */
    def public init_jacobians_file(List<Jacobian> jacs) {
        load(jacs)
        return
        '''
        function «jacobiansStructName» = «initFunctionName»(«initFunctionArgsList()»)

        «init_code()»
        '''
    }
    /**
     * The complete content of the file with Matlab function to update the Jacobians
     */
    def public update_jacobians_file(List<Jacobian> jacs ) {
        load(jacs)
        return
        '''
        function out = «updateFunctionName»(«updateFunctionArgsList()»)

        «update_code()»

        out = «jacobiansStructName»;
        '''
    }

    def private load(List<Jacobian> jacs)
    {
        if(jacs === null) return;
        if( jacs === this.jacs && this.jacsInfo !== null ) return; // already loaded
        this.jacsInfo = new ArrayList<TransformConversionInfo>()
        this.jacs = jacs
        maximaConverter.load(robot, transforms)
        for( J : jacs )
        {
            val textInfo = maximaConverter.process(J)
            val transform= iit.dsl.coord.generator.Common.getInstance.getTransform(transforms, J.baseFrame.name, J.movingFrame.name)
            val argsInfo = new TransformsInfo$Arguments(transform)
            jacsInfo.add( new TransformConversionInfo(argsInfo, Utilities.MatrixType.HOMOGENEOUS, textInfo) )
        }
        maximaConverter.done()
    }

    /**
     * The body of the Matlab function to initialize the Jacobians
     */
    def private init_code()
    {
        val code = new StringConcatenation()
        val jaciter = jacs.iterator
        val infoiter= jacsInfo.iterator
        while( jaciter.hasNext() )
        {
            val J    = jaciter.next
            val info = infoiter.next
            val id = mxId( J );
            code.append( id + " = zeros(" + J.rows + "," + J.cols + ");" )
            code.newLine()
            helperCfg.setCurrentMatrixName( id )
            code.append( helper.matrixInitCode(info.layers, info.basic.constants, maximaReplSpecs) )
            code.newLine()
        }
        return code
    }

    def private update_code()
    {
        val code = new StringConcatenation()
        val jaciter = jacs.iterator
        val infoiter= jacsInfo.iterator
        while( jaciter.hasNext() )
        {
            val J    = jaciter.next
            val info = infoiter.next
            val id = mxId( J );
            helperCfg.setCurrentMatrixName( id )
            code.append( helper.matrixUpdateCode(transforms, info, maximaReplSpecs) )
            code.newLine()
        }
        return code
    }

    def private mxId(Jacobian j) {
        return jacobiansStructName + "." + j.name
    }

    def private updateFunctionArgsList()
    {
        var StringConcatenation ret = new StringConcatenation()
        ret.append(jacobiansStructName)
        for(arg : configurator.getUpdateFunctionArguments() ) {
            ret.append(", " + arg)
        }
        return ret
    }
    def private initFunctionArgsList() {
        val string = new StringConcatenation()
        val iter = configurator.initFunctionArguments.iterator
        while( iter.hasNext ) {
            string.append( iter.next )
            if( iter.hasNext ) {
                string.append(", ")
            }
        }
        return string
    }

    private CharSequence jacobiansStructName = '''jacs'''

    private final Robot robot
    private final Model transforms
    private IConfigurator configurator = null

    private List<Jacobian> jacs = null
    private List<TransformConversionInfo> jacsInfo = null

    private iit.dsl.coord.generator.matlab.GenerationCore.CommonGenConfig helperCfg =
               new iit.dsl.coord.generator.matlab.GenerationCore.CommonGenConfig
    private iit.dsl.coord.generator.MatrixGenCommon helper =
               new iit.dsl.coord.generator.MatrixGenCommon( helperCfg )
    private iit.dsl.coord.generator.MaximaDSLReplacements maximaReplSpecs = null


    private iit.dsl.generator.maxima.Converter  maximaConverter = null
    private iit.dsl.maxdsl.utils.DSLAccessor      maxdslAccess  = null
}