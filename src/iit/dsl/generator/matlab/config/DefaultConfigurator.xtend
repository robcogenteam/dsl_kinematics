package iit.dsl.generator.matlab.config

import java.util.HashMap

import iit.dsl.kinDsl.Robot
import iit.dsl.generator.Common

import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.coord.generator.matlab.VectorBasedVarsAccess
import iit.dsl.generator.matlab.ConstantsAccess

class DefaultConfigurator implements IConfigurator
{
    new(Robot robot,
        iit.dsl.coord.coordTransDsl.Model transformsModel,
        iit.dsl.generator.maxima.IConverterConfigurator maximaConfig )
    {
        this.robot = robot
        this.constsHelper = new ConstantsAccess(robot, doConstantFolding(), constsStructName)
        maximaConverterConfig = maximaConfig
    }

    override getKindslMaximaConverterConfigurator() {
        return this.maximaConverterConfig
    }

    override getMaximaConverterConfigurator() {
        return this.maximaConverterConfig
    }

    override getConstantsValueExprGenerator(Model model) {
        return constsHelper.makeCTDSLConstantsAccess()
    }

    override getVariablesValueExprGenerator(Model model) {
        val map = new HashMap<String,Integer>()
        var i = 1 // Octave/Matlab indices are 1-based
        for( j : robot.joints ) {
            map.put(j.variableName.toString, i)
            i = i+1
        }
        return new VectorBasedVarsAccess(jointStatusName, map)
    }

    override getUpdateFunctionArguments() {
        return updateFArgs
    }
    override getInitFunctionArguments() {
        return #[constsStructName]
    }

    override doConstantFolding() {
        return false
    }

    private Robot robot = null
    private ConstantsAccess constsHelper = null
    private extension Common = Common.getInstance()
    private iit.dsl.generator.maxima.IConverterConfigurator maximaConverterConfig

    private String[] updateFArgs = #[jointStatusName, paramsStructName, constsStructName]

    public static String paramsStructName = "params"
    public static String constsStructName = "consts"
    public static String jointStatusName = "q";

}

