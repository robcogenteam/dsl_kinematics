package iit.dsl.generator.matlab

import iit.dsl.generator.Common.IPField
import iit.dsl.generator.Common.JFField
import iit.dsl.generator.common.Constants.FrameConstant
import iit.dsl.generator.common.Constants.JointConstant
import iit.dsl.generator.common.Constants.LinkConstant
import iit.dsl.generator.common.RobotInfoRegister
import iit.dsl.kinDsl.AbstractLink
import iit.dsl.kinDsl.Joint
import iit.dsl.kinDsl.RefFrame
import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.Var

import static extension iit.dsl.generator.common.Constants.isConst

/**
 * Generator for the Octave/Matlab expressions that would evaluate to
 * the value of a robot property.
 *
 * The purpose of this class is to have a common component that would
 * distinguish whether the property is a parameter or a constant, and, in the
 * case of a constant, deal with the constant-folding configuration option.
 */
class ValueExprBuilder
{
    /**
     * \param consts the name of the struct that it is assumed to have a field
     *        for each constant property of the robot
     * \param params the name of the struct that it is assumed to have a field
     *        for each parameter of the robot model
     * \param folding true if the user wants to apply constant folding, that is,
     *        generate float literals every time code for a constant has to be
     *        generated
     */
    public new(Robot rob, String consts, String params, boolean folding)
    {
        robot = rob
        params= RobotInfoRegister.parameters(robot)
        constsStructName = consts
        paramsStructName = params
        constantsHelper = new ConstantsAccess(robot, folding, constsStructName)
    }

    def public valueExpr(Var property, AbstractLink l, IPField f)
    {
        if( property.isConst ) {
            return constantsHelper.valueExpr( new LinkConstant(l,f) )
        }
        return parameterValue(property)
    }

    def public valueExpr(Var property, Joint j, JFField f)
    {
        if( property.isConst ) {
            return constantsHelper.valueExpr( new JointConstant(j,f) )
        }
        return parameterValue(property)
    }

    def public valueExpr(Var property, RefFrame fr, JFField f)
    {
        if( property.isConst ) {
            val link = iit.dsl.generator.Common.getInstance.getContainingLink(robot, fr)
            return constantsHelper.valueExpr( new FrameConstant(fr,link,f) )
        }
        return parameterValue(property)
    }

    def private parameterValue(Var property)
    {
        val pinfo = params.byVar.get(property)
        if( pinfo !== null ) {
            return Parameters.value(pinfo, paramsStructName)
        }
        throw new RuntimeException("Fatal: the given model Var is neither a constant nor a parameter" )
    }

    private Robot robot
    private iit.dsl.generator.common.Parameters params
    private ConstantsAccess constantsHelper
    private String constsStructName
    private String paramsStructName
}