package iit.dsl.generator.matlab

import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGenerator

import iit.dsl.kinDsl.Robot
import iit.dsl.generator.Jacobian
import iit.dsl.generator.matlab.config.IConfigurator

import static iit.dsl.coord.generator.matlab.Generator.initFunctionName
import static iit.dsl.coord.generator.matlab.Generator.updateFunctionName
import iit.dsl.coord.generator.Utilities

import java.util.ArrayList



class Generator implements IGenerator
{
    private Jacobians  jacGen   = null
    private Jsim       jsimGen  = new Jsim()
    private Transforms transGen = null
    private InertiaProperties inertiaGen = new InertiaProperties()
    private CompositeInertia ciGen = new CompositeInertia()
    private InverseDynamics idGen = new InverseDynamics();
    private ForwardDynamics fdGen = new ForwardDynamics();
    private RoysModel roy = new RoysModel()
    private Constants constsGen = new Constants()

    private IConfigurator configurator = null


    public new(IConfigurator config)
    {
        configurator = config
    }

    override void doGenerate(Resource resource, IFileSystemAccess fsa)
    {
    }

    def generateConstantsFile(Robot robot, IFileSystemAccess fsa) {
        val file = Constants::functionName + ".m"
        fsa.generateFile(file, constsGen.functionBody(robot))
    }
    def generateParametersFile(Robot robot, IFileSystemAccess fsa) {
        val file = Parameters.initFunctionName() + ".m"
        fsa.generateFile(file, Parameters.initFunctionBody(robot))
    }
    def generatePlotFramesFile(Robot robot, IFileSystemAccess fsa,
        iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        val PlotFrames gen = new PlotFrames(robot, transformsModel)
        fsa.generateFile(PlotFrames::functionName + ".m"  , gen.plotFramesCode())
    }

    def generateTransformsFiles(
        Robot robot,
        IFileSystemAccess fsa,
        iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        transGen = new Transforms(robot, transformsModel, configurator);

        var iit.dsl.coord.generator.Utilities$MatrixType mxType
        var String fileName

        mxType   = iit.dsl.coord.generator.Utilities$MatrixType::HOMOGENEOUS;

        fileName = initFunctionName(mxType) + ".m"
        fsa.generateFile(fileName  , transGen.homogeneous_init_fileContent(robot))
        fileName = updateFunctionName(mxType) + ".m"
        fsa.generateFile(fileName, transGen.homogeneous_update_fileContent(robot))


        mxType   = iit.dsl.coord.generator.Utilities$MatrixType::_6D;

        fileName = initFunctionName(mxType) + ".m"
        fsa.generateFile(fileName, transGen.motion6D_init_fileContent(robot))
        fileName = updateFunctionName(mxType) + ".m"
        fsa.generateFile(fileName, transGen.motion6D_update_fileContent(robot))


        mxType   = iit.dsl.coord.generator.Utilities$MatrixType::_6D_FORCE;

        fileName = initFunctionName(mxType) + ".m"
        fsa.generateFile(fileName, transGen.force6D_init_fileContent(robot))
        fileName = updateFunctionName(mxType) + ".m"
        fsa.generateFile(fileName, transGen.force6D_update_fileContent(robot))
    }


    //  JACOBIANS //
    def public generateJacobiansFiles(
        Robot robot,
        IFileSystemAccess fsa,
        iit.dsl.transspecs.transSpecs.DesiredTransforms desired,
        iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        val jacobians = new ArrayList<Jacobian>()
        if(desired !== null) {
            if(desired.jacobians !== null) {
                for(iit.dsl.transspecs.transSpecs.FramePair jSpec : desired.jacobians.getSpecs()) {
                    jacobians.add(new Jacobian(robot, jSpec))
                }
            }
        }
        jacGen = new Jacobians( robot, transformsModel, configurator )
        fsa.generateFile(
            Jacobians::initFunctionName + ".m",
            jacGen.init_jacobians_file(jacobians))
        fsa.generateFile(
            Jacobians::updateFunctionName + ".m",
            jacGen.update_jacobians_file(jacobians))
    }

    def generateJSIMFiles(
        Robot robot,
        iit.dsl.coord.coordTransDsl.Model transformsModel,
        IFileSystemAccess fsa)
    {
        fsa.generateFile(Jsim::updateFunctionName + ".m", jsimGen.jsim_update_code(robot, transformsModel))
        fsa.generateFile(Jsim::invertFunctionName + ".m", jsimGen.jsim_inverse_code(robot))
    }

    def generateIDFiles(Robot robot,
        iit.dsl.coord.coordTransDsl.Model transformsModel,
        IFileSystemAccess fsa)
    {
        fsa.generateFile(InverseDynamics::functionName + ".m",
            idGen.body(robot, transformsModel) )
    }

    def generateFDFiles(Robot robot,
        iit.dsl.coord.coordTransDsl.Model transformsModel,
        IFileSystemAccess fsa)
    {
        fsa.generateFile(ForwardDynamics::functionName + ".m",
            fdGen.body(robot, transformsModel) )
    }

    def generateCommonDynamicsFiles(Robot robot,
        iit.dsl.coord.coordTransDsl.Model transformsModel,
        IFileSystemAccess fsa)
    {
        fsa.generateFile(InertiaProperties::functionName + ".m",
            inertiaGen.functionBody(robot, configurator.doConstantFolding()) )

        fsa.generateFile(CompositeInertia::functionName + ".m",
            ciGen.functionBody(robot, transformsModel)  )
    }

    def generateRoysModel(Robot robot, IFileSystemAccess fsa) {
        val fpath = RoysModel::functionName(robot)+".m"
        fsa.generateFile(fpath, roy.functionBody(robot, configurator.doConstantFolding()))
    }

    /**
     * This template generates a matlab file with the initialization of
     * a structure describing the robot kinematics, according to the model described
     * in "A beginner's guide to &-D vectors (part 2)" by Roy Featherstone.
     */
    @Deprecated
    def featherstoneMatlabModel(Robot robot) {
        roy.functionBody(robot, configurator.doConstantFolding())
    }
}