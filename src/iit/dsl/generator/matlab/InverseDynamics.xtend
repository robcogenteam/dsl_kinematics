package iit.dsl.generator.matlab

import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.Link
import iit.dsl.kinDsl.AbstractLink
import iit.dsl.coord.generator.Utilities
import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.kinDsl.Joint
import org.eclipse.xtend2.lib.StringConcatenation

class InverseDynamics
{
    public static CharSequence functionName = '''inverseDynamics'''

    // TODO: possibly, in the future, optimize the spatial cross product (i.e.
    //  avoid code that computes the whole 6x6 matrix)

    public def body(Robot robot, Model transforms) '''
        «IF robot.base.floating»
            function [tau «robot.base.acceleration»] = «functionName»(«ipVarName», «xmVarName», «robot.base.velocity», gravity, qd, qdd, fext)
        «ELSE»
            function tau = «functionName»(«ipVarName», «xmVarName», qd, qdd, fext)
            g = 9.81;
        «ENDIF»
        «val sortedLinks = robot.links.sortBy(link | getID(link))»
        «IF robot.base.floating»
            if nargin < 7
                «FOR l : robot.linksAndBase»
                    fext{«l.ID+1»} = zeros(6,1);
                «ENDFOR»
            end
        «ELSE»
            if nargin < 5
                «FOR l : sortedLinks»
                    fext{«l.ID»} = zeros(6,1);
                «ENDFOR»
            end
        «ENDIF»
        %
        % Pass 1. Forward propagate velocities and accelerations
        %
        «val transformsMap = Common::getParentToChildTransforms(robot, transforms, Utilities$MatrixType::_6D, xmVarName)»
        «FOR Link l : sortedLinks»
            «val parent   = l.parent»
            «val myJoint  = l.connectingJoint»
            «val velocity = l.velocity»
            «val acceler  = l.acceleration»
            «val child_X_parent = transformsMap.get(l)»
            «val jid         = Common::arrayIndex(myJoint)»
            «val subspaceIdx = Common::spatialVectorIndex(myJoint)»

            % Link '«l.name»'
            «IF robot.base.floating»
                «velocity» = «child_X_parent» * «parent.velocity»;
                «velocity»(«subspaceIdx») = «velocity»(«subspaceIdx») + qd(«jid»);

                vcross = vcross_mx(«velocity»);

                «IF parent.equals(robot.base)/* parent is the floating base */»
                    «acceler» = vcross(:,«subspaceIdx») * qd(«jid»);
                «ELSE»
                    «acceler» = «child_X_parent» * «parent.acceleration» + (vcross(:,«subspaceIdx») * qd(«jid»));
                «ENDIF»
                «acceler»(«subspaceIdx») = «acceler»(«subspaceIdx») + qdd(«jid»);

                «l.force» = -fext{«l.ID+1»} + «l.spatialInertia» * «acceler» + (-vcross' * «l.spatialInertia» * «velocity»);
            «ELSE»
                «IF parent.equals(robot.base)»
                    «acceler» = «child_X_parent»(:,6) * g; % TODO hide 6
                    «acceler»(«subspaceIdx») = «acceler»(«subspaceIdx») + qdd(«jid»);
                    «velocity» = «spatialVelDueToJointOnly(myJoint, "qd("+jid+")")»;
                    «IF myJoint.prismatic»
                        % The first joint is prismatic, no centripetal terms.
                        «l.force» = -fext{«l.ID»} + «l.spatialInertia» * «acceler»;
                    «ELSE»
                        w2 = qd(«jid»)*qd(«jid»);
                        vxIv = [-«l.spatialInertia»(2,3) * w2; ...
                                 «l.spatialInertia»(1,2) * w2; ...
                                 0; ...
                                 «l.spatialInertia»(2,6) * w2; ...
                                 «l.spatialInertia»(3,4) * w2; ...
                                 0];
                        «l.force» = -fext{«l.ID»} + «l.spatialInertia» * «acceler» + vxIv;
                    «ENDIF»
                «ELSE»
                    «velocity» = ((«child_X_parent») * «parent.velocity»);
                    «velocity»(«subspaceIdx») = «velocity»(«subspaceIdx») + qd(«jid»);

                    vcross = vcross_mx(«velocity»);

                    «acceler» = «child_X_parent» * «parent.acceleration» + (vcross(:,«subspaceIdx») * qd(«jid»));
                    «acceler»(«subspaceIdx») = «acceler»(«subspaceIdx») + qdd(«jid»);

                    «l.force» = -fext{«l.ID»} + «l.spatialInertia» * «acceler» + (-vcross' * «l.spatialInertia» * «velocity»);
                «ENDIF»
            «ENDIF»
        «ENDFOR»

        «IF robot.base.floating»
            %
            % The force exerted on the floating base by the links
            %
            «val base = robot.base»
            vcross = vcross_mx(«base.velocity»);
            «base.force» = -fext{1} - vcross' * «base.spatialInertia» * «base.velocity»;


            %
            % Pass 2. Compute the composite inertia and the spatial forces
            %
            ci = «CompositeInertia::functionName»(«ipVarName», «xmVarName», 'motion');
            «FOR l : sortedLinks.reverseView()»
                «val parent = l.parent»
                «val child_X_parent = transformsMap.get(l)»
                «parent.force» = «parent.force» + «child_X_parent»' * «l.force»;
            «ENDFOR»

            %
            % The base acceleration due to the force due to the movement of the links
            %
            «robot.base.acceleration» = - inverse(ci.«robot.base.inertiaC») * «robot.base.force»; % TODO inverse

            %
            % Pass 3. Compute the joint forces while propagating back the floating base acceleration
            %
            tau = zeros(«robot.DOFs - 6», 1);
            «FOR l : sortedLinks»
                «val parent = l.parent»
                «val joint  = l.connectingJoint»
                «val child_X_parent = transformsMap.get(l)»
                «val idx = Common::spatialVectorIndex(joint)»
                «l.acceleration» = «child_X_parent» * «parent.acceleration»;
                tau(«Common::arrayIndex(joint)») = ci.«l.inertiaC»(«idx»,:) * «l.acceleration» + «l.force»(«idx»);

            «ENDFOR»

            «robot.base.acceleration» = «robot.base.acceleration» + gravity;
        «ELSE»
            %
            % Pass 2. Compute the joint torques while back propagating the spatial forces
            %
            tau = zeros(«robot.DOFs»,1);
            «FOR l : sortedLinks.reverseView»

                % Link '«l.name»'
                «val parent = l.parent»
                «val joint  = l.connectingJoint»
                tau(«Common::arrayIndex(joint)») = «l.force»(«Common::spatialVectorIndex(joint)»);
                «IF ( ! parent.equals(robot.base))»
                    «val child_X_parent = transformsMap.get(l)»
                    «parent.force» = «parent.force» + «child_X_parent»' * «l.force»;
                «ENDIF»
            «ENDFOR»
        «ENDIF»
        end

        function vc = vcross_mx(v)
            vc = [   0    -v(3)  v(2)   0     0     0    ;
                     v(3)  0    -v(1)   0     0     0    ;
                    -v(2)  v(1)  0      0     0     0    ;
                     0    -v(6)  v(5)   0    -v(3)  v(2) ;
                     v(6)  0    -v(4)   v(3)  0    -v(1) ;
                    -v(5)  v(4)  0     -v(2)  v(1)  0    ];
        end
    '''

    def private spatialInertia(AbstractLink l) {
        return ipVarName + "." + InertiaProperties.StructFields.link(l) + "." +
                                 InertiaProperties.StructFields.stensor
    }

    /*
     * Return text like "[0;0;qd(1);0;0;0]" to initialize the spatial velocity
     * of a child of a fixed base.
     */
    def private spatialVelDueToJointOnly(Joint j, String jVel)
    {
        val text = new StringConcatenation
        text.append("[")
        val svi = Common.spatialVectorIndex(j)
        for(var i=1; i<=6; i++) {
            if(i == svi) {
                text.append(jVel)
            } else {
                text.append("0")
            }
            if(i<6) text.append(";")
        }
        text.append("]")
        return text
    }

    extension iit.dsl.generator.Common common = iit.dsl.generator.Common::getInstance()
    extension SpatialQuantitiesNames varnames = SpatialQuantitiesNames::getInstance()

    private static String xmVarName = "xm"
    private static String ipVarName = "ip"
}