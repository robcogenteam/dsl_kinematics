package iit.dsl.generator.matlab

import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.AbstractLink
import iit.dsl.kinDsl.PrismaticJoint

import iit.dsl.coord.generator.Utilities

class ForwardDynamics
{
    def static functionName() '''forwardDynamics'''

    public def body(Robot robot, iit.dsl.coord.coordTransDsl.Model transforms)
    '''
        «IF robot.base.floating»
            function [qdd «robot.base.acceleration»] = «functionName»(«ipVarName», «xmVarName», «robot.base.velocity», gravity, qd, tau, fext)
        «ELSE»
            function qdd = «functionName»(«ipVarName», «xmVarName», qd, tau, fext)
            g = 9.81;
        «ENDIF»
        qdd = zeros(«robot.jointDOFs»,1);
        «val sortedLinks = robot.links.sortBy(link | getID(link))»
        «IF robot.base.floating»
            «robot.base.inertiaA» = «robot.base.sTensor»;
        «ENDIF»
        «IF !robot.base.floating»
            «sortedLinks.get(0).velocity» = zeros(6,1);
        «ENDIF»
        «FOR l : sortedLinks»
            «l.inertiaA» = «l.sTensor»;
        «ENDFOR»
        «IF robot.base.floating»
            if nargin > 6
                «robot.base.biasForceName» = - fext{1};
                «FOR l : sortedLinks»
                    «l.biasForceName» = - fext{«l.ID+1»};
                «ENDFOR»
            else
                «FOR l : robot.linksAndBase»
                    «l.biasForceName» = zeros(6,1);
                «ENDFOR»
            end
        «ELSE»
            if nargin > 4
                «FOR l : sortedLinks»
                    «l.biasForceName» = - fext{«l.ID»};
                «ENDFOR»
            else
                «FOR l : sortedLinks»
                    «l.biasForceName» = zeros(6,1);
                «ENDFOR»
            end
        «ENDIF»
        %% ---------------------- FIRST PASS ----------------------
        %% Note that, during the first pass, the articulated inertias are really
        %% just the spatial inertia of the links (see assignments above).
        %%  Afterwards things change, and articulated inertias shall not be used
        %%  in functions which work specifically with spatial inertias.
        «val transformsMap = Common::getParentToChildTransforms(robot, transforms, Utilities$MatrixType::_6D, xmVarName)»
        «FOR l : sortedLinks»
            «val parent   = l.parent»
            «val joint    = l.connectingJoint»
            «val velocity = l.velocity»
            «val cterm    = l.cTermName»
            «val biasF    = l.biasForceName»
            «val jid      = Common::arrayIndex(joint)»
            «val child_X_parent = transformsMap.get(l)»
            «val subspaceIdx    = Common::spatialVectorIndex(joint)»

            «IF parent.equals(robot.base) && (!robot.base.floating)»
                «velocity»(«subspaceIdx») = qd(«jid»);

                «IF joint.prismatic»
                    % The first joint is prismatic, no bias force term
                «ELSE»
                    «biasF» = «biasF» + vxIv(qd(«jid»), «l.inertiaA»);
                «ENDIF»
            «ELSE»
                «velocity» = («child_X_parent») * «parent.velocity»;
                «velocity»(«subspaceIdx») = «velocity»(«subspaceIdx») + qd(«jid»);

                vcross = vcross_mx(«velocity»);
                «cterm» = vcross(:,«subspaceIdx») * qd(«jid»);
                «biasF» = «biasF» + -vcross' * «l.inertiaA» * «velocity»; %%% vxIv(«velocity», «l.inertiaA»);
            «ENDIF»
        «ENDFOR»

        «IF robot.base.floating»
            vcross = vcross_mx(«robot.base.velocity»);
            «robot.base.biasForceName» = «robot.base.biasForceName» + -vcross' * «robot.base.inertiaA» * «robot.base.velocity»;
        «ENDIF»

        %% ---------------------- SECOND PASS ----------------------
        IaB = zeros(6,6);
        pa  = zeros(6,1);
        «FOR l : sortedLinks.reverseView»
            «val joint = l.connectingJoint»
            «val subspaceIdx = Common::spatialVectorIndex(joint)»
            «val U = l.UTermName»
            «val D = l.DTermName»
            «val u = l.uTermName»
            «val p = l.biasForceName»
            «val I = l.inertiaA»
            «val parent = l.parent»
            «val child_X_parent = transformsMap.get(l)»

            «u» = tau(«Common::arrayIndex(joint)») - «p»(«subspaceIdx»);
            «U» = «I»(:,«subspaceIdx»);
            «D» = «U»(«subspaceIdx»);

            «IF !parent.equals(robot.base) || robot.base.floating»
                «IF joint instanceof PrismaticJoint»
                    Ia_p = «I» - «U»/«D» * «U»';
                    pa = «p» + Ia_p * «cTermName(l)» + «U» * «u»/«D»;
                    IaB = «child_X_parent»' * Ia_p * «child_X_parent»;          %% ctransform_Ia_prismatic(Ia_p, «child_X_parent», IaB);
                «ELSE»
                    Ia_r = «I» - «U»/«D» * «U»';
                    pa = «p» + Ia_r * «cTermName(l)» + «U» * «u»/«D»;
                    IaB = «child_X_parent»' * Ia_r * «child_X_parent»;          %%ctransform_Ia_revolute(Ia_r, «child_X_parent», IaB);
                «ENDIF»
                «parent.inertiaA» = «parent.inertiaA» + IaB;
                «parent.biasForceName» = «parent.biasForceName» + («child_X_parent»)' * pa;
            «ENDIF»
        «ENDFOR»

        «IF robot.base.floating»
            % + The acceleration of the floating base «robot.base.name», without gravity
            «robot.base.acceleration» = - «robot.base.inertiaA» \ «robot.base.biasForceName»;
        «ENDIF»

        % ---------------------- THIRD PASS ----------------------
        «FOR l : sortedLinks»
            «val parent = l.parent»
            «val joint  = l.connectingJoint»
            «val jid    = Common::arrayIndex(joint)»
            «val child_X_parent = transformsMap.get(l)»
            «val subspaceIdx = Common::spatialVectorIndex(joint)»
            «IF parent.equals(robot.base) && (!robot.base.floating)»
                «l.acceleration» = («child_X_parent»)(:,6) * g;  % (:,6) = column of linear Z
            «ELSE»
                «l.acceleration» = («child_X_parent») * «acceleration(parent)» + «cTermName(l)»;
            «ENDIF»
            qdd(«jid») = («uTermName(l)» - dot(«l.UTermName»,«l.acceleration»)) / «DTermName(l)»;
            «l.acceleration»(«subspaceIdx») = «l.acceleration»(«subspaceIdx») + qdd(«jid»);
        «ENDFOR»

        «IF robot.base.floating»
            % + Add gravity to the acceleration of the floating base
            «robot.base.acceleration» = «robot.base.acceleration» + gravity;
        «ENDIF»
        end

        function ret = vxIv(omegaz, I)
            wz2 = omegaz*omegaz;
            ret = zeros(6,1);
            ret(1) = -I(2,3) * wz2;
            ret(2) =  I(1,2) * wz2;
            %%ret(3) =  0;
            ret(4) =  I(2,6) * wz2;
            ret(5) =  I(3,4) * wz2;
            %%ret(6) =  0;
        end

        function vc = vcross_mx(v)
            vc = [   0    -v(3)  v(2)   0     0     0    ;
                     v(3)  0    -v(1)   0     0     0    ;
                    -v(2)  v(1)  0      0     0     0    ;
                     0    -v(6)  v(5)   0    -v(3)  v(2) ;
                     v(6)  0    -v(4)   v(3)  0    -v(1) ;
                    -v(5)  v(4)  0     -v(2)  v(1)  0    ];
        end
    '''

    def private sTensor(AbstractLink l) {
        return ipVarName + "." + InertiaProperties.StructFields.link(l) + "." +
                                 InertiaProperties.StructFields.stensor
    }

    def private cTermName(AbstractLink l)  { return l.name + "_c" }
    def private biasForceName(AbstractLink l)  { return l.name + "_p" }
    def private UTermName(AbstractLink l)  { return l.name + "_U" }
    def private DTermName(AbstractLink l)  { return l.name + "_D" }
    def private uTermName(AbstractLink l)  { return l.name + "_u" }

    extension iit.dsl.generator.Common common = iit.dsl.generator.Common::getInstance()
    extension SpatialQuantitiesNames varnames = SpatialQuantitiesNames::getInstance()
    private static String xmVarName = "xm"
    private static String ipVarName = "ip"
}