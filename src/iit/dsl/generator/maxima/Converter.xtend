package iit.dsl.generator.maxima

import iit.dsl.coord.generator.ProcessedMatrixInfo
import iit.dsl.kinDsl.Robot
import iit.dsl.generator.Jacobian

/**
 * This class should be used to convert the output of the Maxima code generated
 * by the generator in this package, to some other form.
 */
class Converter
{
    /**
     * Default constructor, uses the default configurator.
     */
    public new() {
        this(new DefaultConfigurator)
    }

    /**
     * @param config the iit.dsl.generator.maxima.IConverterConfigurator to be
     *        used by this instance.
     */
    public new(IConverterConfigurator config)
    {
        transformsConverter = new iit.dsl.coord.generator.maxima.converter.MaximaConverter(config)
        maximaLibJacobians   = config.libsPath + "/" + config.jacobiansLibName
        maximaLibUtils       = config.libsPath + "/" + config.utilsLibName
        maximaJacobiansPath  = config.generatedCodeLocation
    }

    def public load(Robot robot, iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        if(transformsModel === null) {
            throw new RuntimeException("The given transforms-model is null. Aborting")
        }
        // this one will also run-batch the scripts for the homog transforms
        transformsConverter.setModel( transformsModel, iit.dsl.coord.generator.Utilities.MatrixType.HOMOGENEOUS )

        val maximaRunner = transformsConverter.maximaRunner // non null only after the previous 'setModel'
        maximaRunner.runBatch(maximaLibJacobians)
        maximaRunner.runBatch(maximaLibUtils)
        maximaRunner.runBatch(maximaJacobiansPath +  "/" + Jacobians::fileName(robot))
        loaded = true
    }

    def public done()
    {
        transformsConverter.done()
        loaded = false
    }

    def public ProcessedMatrixInfo process(Jacobian J)
    {
        if(!loaded) {
            throw new RuntimeException("Can't do anything before a <Robot,TransformsModel> are loaded")
        }
        val jacobian = J.getName()+ "(" + J.getArgsList() + ")"
        return transformsConverter.process(jacobian, 6, J.cols)
    }


    //filesytem location where to find required code to set up the environment
    private String maximaLibJacobians   = null
    private String maximaLibUtils       = null
    private String maximaJacobiansPath  = null
    private boolean loaded = false;
    private iit.dsl.coord.generator.maxima.converter.MaximaConverter transformsConverter = null
}

class DefaultConfigurator
    extends iit.dsl.coord.generator.maxima.converter.DefaultConfigurator
    implements IConverterConfigurator
{
    override getUtilsLibName() {
        return "utils"
    }

    override getJacobiansLibName() {
        return "jacobians"
    }
}
