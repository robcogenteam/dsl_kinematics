package iit.dsl.generator.sl.robot.math

import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.RevoluteJoint
import iit.dsl.kinDsl.PrismaticJoint
import iit.dsl.kinDsl.Joint
import iit.dsl.kinDsl.AbstractLink
import iit.dsl.kinDsl.Vector3
import iit.dsl.generator.common.Vector3D
import iit.dsl.generator.sl.Common
import iit.dsl.generator.common.TreeUtils
import iit.dsl.generator.common.Parameters
import iit.dsl.generator.cpp.CppParameters
import iit.dsl.coord.coordTransDsl.Model
import iit.dsl.generator.common.Parameters.ParameterInfo
import iit.dsl.generator.common.Parameters.KinematicParameter
import iit.dsl.generator.cpp.ConstantsAccess
import iit.dsl.kinDsl.Var
import iit.dsl.generator.common.Constants.ConstantInfo
import iit.dsl.generator.common.RobotInfoRegister

class Headers {

    def public gjac_declare(Robot robot) '''
        // This is a set of flags used by SL to determine which joint (column
        //  index of this matrix) contributes to the velocity of which
        //  end-effector (row index)
        int Jlist[N_ROBOT_ENDEFFECTORS][N_ROBOT_DOFS];
    '''
    def public gjac_math(Robot robot) {
        val code = new StringBuffer
        val leafs = robot.chainEndLinks
        var ee_i = 1
        for(leaf : leafs) {
            code.append('''bzero(Jlist[«ee_i»], sizeof(Jlist[«ee_i»]));''')
            code.append("\n");
            val chain = TreeUtils::chainToBase(leaf)
            for(l : chain) {
               code.append('''Jlist[«ee_i»][«Common::jointEnumID(l.connectingJoint)»] = 1;''')
               code.append("\n");
            }
            ee_i = ee_i + 1
        }
        return code
    }

    def public contact_gjac_declare(Robot robot) '''
        // This is a set of flags used by SL to determine which joint (column
        //  index of this matrix) contributes to the velocity of which
        //  link (row index)
        int Jlist[N_ROBOT_LINKS][N_ROBOT_DOFS];
    '''
    def public contact_gjac_math(Robot robot) {
        val code = new StringBuffer
        for(link : robot.links) {
            val lID = Common::linkEnumID(link)
            code.append('''bzero(Jlist[«lID»], sizeof(Jlist[«lID»]));''')
            code.append("\n");
            val chain = TreeUtils::chainToBase(link)
            for(l : chain) {
               code.append('''Jlist[«lID»][«Common::jointEnumID(l.connectingJoint)»] = 1;''')
               code.append("\n");
            }
        }
        return code
    }

    def public prismatic_joints(Robot robot) '''
        «FOR j : robot.joints»
            prismatic_joint_flag[«j.arrayIdx + 1»] = «j.prismaticFlag»;
        «ENDFOR»
    '''
    def private dispatch prismaticFlag(RevoluteJoint joint) '''0'''
    def private dispatch prismaticFlag(PrismaticJoint joint)'''1'''

    def public floating_base(Robot robot)
    '''const int floating_base_flag = «IF robot.base.floating»1«ELSE»0«ENDIF»;'''

    def public opengl(Robot robot, Model transforms, boolean constantFolding) {
        return new OpenGL_h(robot, transforms, constantFolding).body()
    }

    private extension iit.dsl.generator.Common common = new iit.dsl.generator.Common
}


/**
 * Class dedicated to the generation of OpenGL.h
 */
class OpenGL_h
{
    new(Robot r, Model transforms, boolean constantFolding) {
        robot = r
        paramsHelper = new CppParameters(RobotInfoRegister.parameters(robot), transforms)
        constsHelper = new ConstantsAccess(robot, constantFolding)
    }

    def public body()
        '''
        «val ns = iit::dsl::generator::cpp::Common::enclosingNamespacesQualifier(robot)»
        #define RAD2DEG (57.3)

        static double x,y,z; // support vars
        «IF paramsHelper.robotParams.isParametric»
            //
            // *** WARNING ***
            // the robot model appears to be parametric
            // code to update the parameters should be added here
            // TODO fix this in RobCoGen
            //
            «IF paramsHelper.robotParams.angles.size > 0»
                «ns»::«paramsHelper.anglesStructType()» _angles;
            «ENDIF»
            «IF paramsHelper.robotParams.lengths.size > 0»
                «ns»::«paramsHelper.lengthsStructType()» _lengths;
            «ENDIF»
            «FOR l : paramsHelper.robotParams.lengths»
                //_lengths.«paramsHelper.structField(l)» = get_«l.name»(); // TODO fix in RobCoGen generator
            «ENDFOR»
            «FOR l : paramsHelper.robotParams.angles»
                //_angles.«paramsHelper.structField(l)» =  get_«l.name»(); // TODO fix in RobCoGen generator
            «ENDFOR»
        «ENDIF»

        // The state of the base
        glPushMatrix();
        glTranslated((GLdouble)basec[0].x[1],(GLdouble)basec[0].x[2],(GLdouble)basec[0].x[3]);
        glRotated((GLdouble)114.5916*ArcCos(baseo[0].q[1]),(GLdouble)baseo[0].q[2],(GLdouble)baseo[0].q[3],(GLdouble)baseo[0].q[4]);

        «opengl_depthVisit(robot.base)»

        // pops the first matrix related to the state of the base
        glPopMatrix();
        '''


    def private CharSequence opengl_depthVisit(AbstractLink link) '''
        «IF link.childrenList.children.size == 0»
            // Draw the end effector
            glPushMatrix();
            x = eff[«opengl_ee_count = opengl_ee_count + 1»].x[_X_];
            y = eff[«opengl_ee_count»].x[_Y_];
            z = eff[«opengl_ee_count»].x[_Z_];
            glRotated(
                    (GLdouble)RAD2DEG*acos(z),(GLdouble)-y,(GLdouble)x,(GLdouble)0);
            myDrawGLElement(«100+opengl_ee_count», (double)Sqrt(x*x + y*y + z*z), 0);
            glPopMatrix();
        «ENDIF»
        «FOR childSpec : link.childrenList.children»
            «val j = childSpec.joint»
            // Joint «j.name»

            glPushMatrix();
            «drawJointCode(j)»

            «opengl_depthVisit(childSpec.link)»

            glPopMatrix();
        «ENDFOR»
    '''
    private int opengl_ee_count = 0;


    def private drawJointCode(Joint joint)
        '''
        «val vec = asVString(joint.refFrame.translation)»
        «val rot = asVString(joint.refFrame.rotation)»
        glPushMatrix();
        // Align the Z axis along the direction between the two joints, to display
        //  the link correctly ('myDrawGLElement()' draws along the Z axis)
        «alignZwithLink(joint)»
        myDrawGLElement(::«Common::jointEnumID(joint)», «distance(joint)», 1);
        glPopMatrix();

        // move to the next joint, the same parameters as in the kinematics model file
        glTranslated((GLdouble)«vec.x», (GLdouble)«vec.y», (GLdouble)«vec.z»);
        glRotated((GLdouble)(RAD2DEG*«rot.x»), (GLdouble)1.0, (GLdouble)0.0, (GLdouble)0.0);
        glRotated((GLdouble)(RAD2DEG*«rot.y»), (GLdouble)0.0, (GLdouble)1.0, (GLdouble)0.0);
        glRotated((GLdouble)(RAD2DEG*«rot.z»), (GLdouble)0.0, (GLdouble)0.0, (GLdouble)1.0);

        // move according to the joint state
        «jointStateMove(joint)»
        '''

    def private dispatch jointStateMove(RevoluteJoint joint)
        '''glRotated((GLdouble)RAD2DEG*state[::«Common::jointEnumID(joint)»].th,(GLdouble)0.0, (GLdouble)0.0, (GLdouble)1.0);'''

    def private dispatch jointStateMove(PrismaticJoint joint)
        '''glTranslated((GLdouble)0.0, (GLdouble)0.0, (GLdouble)state[::«Common::jointEnumID(joint)»].th);'''

    def private alignZwithLink(Joint j)
    {
        var CharSequence firstItem
        val pos = j.refFrame.translation
        // If there is no translation along x nor y, then the current OpenGL frame
        //  has already the z axis aligned with the line connecting the two joints
        if( !Parameters::isParameter(pos.x)  &&
            !Parameters::isParameter(pos.y) )
        {
            if(pos.x.asFloat == 0.0  &&  pos.y.asFloat == 0.0) {
                return '''// nothing to do'''
            }
        }

        val str = asVString(pos)
        firstItem = '''(RAD2DEG*std::acos( «str.z»/(«str.norm()») ))'''

        return
        '''glRotated((GLdouble)«firstItem», (GLdouble)-(«str.y»), (GLdouble)«str.x», (GLdouble)0.0);'''
    }


    def private distance(Joint j) {
        val vec = j.refFrame.translation
        if( ! Parameters::isParametric(vec) ) {
            return Vector3D::norm(vec)
        } else {
            return asVString(vec)
        }
    }

    def private VectorString asVString(Vector3 vec) {
        return new VectorString(value(vec.x), value(vec.y), value(vec.z))
    }

    def private String value(Var v)
    {
        // parameter
        val pinfo = paramsHelper.robotParams.byVar.get(v)
        if( pinfo !== null ) {
            var sign = ""
            if(pinfo.emfObj.minus) sign = "-"
            return sign+value(pinfo)
        }
        // constant
        val cinfo = constsHelper.robotConsts.byVar.get(v)
        if( cinfo !== null ) {
            return value(cinfo)
        }
        // float literal
        return v.asFloat.toString
    }

    def private value(ParameterInfo p)
    {
        val pkin = p as KinematicParameter // it must not be an inertia parameter
        if(pkin.isRotation) {
            return "_angles." + paramsHelper.structField(p)
        } else {
            return "_lengths." + paramsHelper.structField(p)
        }
    }
    def private value(ConstantInfo c) {
        return constsHelper.valueExpr(c)
    }

    private Robot robot
    private CppParameters paramsHelper = null
    private ConstantsAccess constsHelper = null
    private extension iit.dsl.generator.Common common = new iit.dsl.generator.Common

    private static class VectorString {
        public String x
        public String y
        public String z

        new(String xx, String yy, String zz) {
            x=xx; y=yy; z=zz;
        }

        def public norm() {
            return '''std::sqrt(«x»*«x» + «y»*«y» + «z»*«z»)'''
        }
    }
}

