package iit.dsl.generator

import iit.dsl.kinDsl.Robot
import iit.dsl.kinDsl.Joint
import iit.dsl.kinDsl.PILiteral
import iit.dsl.kinDsl.FloatLiteral
import iit.dsl.kinDsl.DivExpr
import iit.dsl.kinDsl.MultExpr
import iit.dsl.kinDsl.PrismaticJoint
import iit.dsl.kinDsl.RevoluteJoint
import iit.dsl.kinDsl.RotoTrasl
import iit.dsl.kinDsl.ParameterLiteral
import iit.dsl.kinDsl.PlainExpr

import iit.dsl.generator.common.Parameters
import iit.dsl.generator.Common.JFField

import org.eclipse.xtend2.lib.StringConcatenation
import iit.dsl.generator.common.RobotInfoRegister
import iit.dsl.generator.common.Constants
import org.eclipse.xtext.nodemodel.util.NodeModelUtils

/**
 * Generator of documents of the Motions-DSL, modeling the location of the joint
 * frames of the given robot.
 *
 * This class is the bridge between the Kinematics-DSL (ie. a robot model) and
 * the code generation infrastructure of the Motions-DSL and Transforms-DSL.
 */
class MotionDSLDocGenerator
{
    def public static String fileName(Robot robot) {
        return robot.name + "_frames.motdsl"
    }

    private extension Common common = Common::getInstance()

    private boolean constantFolding = false // default value

    def public documentContent(Robot robot) {
        return documentContent(robot, constantFolding)
    }

    def public documentContent(Robot robot, boolean cfolding)
    {
        constantFolding = cfolding
        val params = RobotInfoRegister.parameters(robot)
        return '''
        Model «robot.name»
        Frames {
            «val alllinks = common.abstractLinks(robot)»
            «FOR link : alllinks
            SEPARATOR ", "
            AFTER ","»«common.getFrameName(link)»«ENDFOR»

            «FOR link : alllinks»
                «FOR frame : link.frames
                SEPARATOR ", " AFTER ","»«frame.name»«ENDFOR»
            «ENDFOR»

            «FOR joint : robot.joints
            SEPARATOR ", "»«common.getFrameName(joint)»«ENDFOR»
        }

        Params «Parameters::lengthsGroupName» {
            «FOR p : params.lengths SEPARATOR ", "»«p.name»«ENDFOR»
        }
        Params «Parameters::anglesGroupName» {
            «FOR p : params.angles SEPARATOR ", "»«p.name»«ENDFOR»
        }

        Convention = local

        «FOR joint : robot.joints»
            «joint.predecessorLink.frameName» -> «joint.frameName» : «motionStepsFromPredecessor(joint)»
        «ENDFOR»

        «FOR joint : robot.joints»
            «joint.frameName» -> «joint.successorLink.frameName» : «motionStepsToSuccessor(joint)»
        «ENDFOR»

        «FOR link : alllinks»
            «FOR frame : link.frames»
                «link.frameName» -> «frame.name» : «motionSteps(frame.transform, frame.name)»
            «ENDFOR»
        «ENDFOR»
    '''
    }

    def private motionStepsFromPredecessor(Joint joint) {
        return motionSteps(joint.refFrame, joint.name)
    }

    def private motionSteps(RotoTrasl rotoTransl, String containerName)
    {
        val StringConcatenation text = new StringConcatenation()
        val transl = rotoTransl.translation
        val rot    = rotoTransl.rotation

        var CharSequence tmp
        var isEmpty = true
        // Translation
        tmp = value(transl.x, JFField.TX, containerName)
        if(tmp.length() > 0) {
            text.append('''trx(«tmp»)''')
            isEmpty = false
        }

        tmp = value(transl.y, JFField.TY, containerName)
        if(tmp.length() > 0) {
            if(!isEmpty) text.append(" ")
            text.append('''try(«tmp»)''')
            isEmpty = false
        }

        tmp = value(transl.z, JFField.TZ, containerName)
        if(tmp.length() > 0) {
            if(!isEmpty) text.append(" ")
            text.append('''trz(«tmp»)''')
            isEmpty = false
        }

        //Rotation
        tmp = value(rot.x, JFField.RX, containerName)
        if(tmp.length() > 0) {
            if(!isEmpty) text.append(" ")
            text.append('''rotx(«tmp»)''')
            isEmpty = false
        }

        tmp = value(rot.y, JFField.RY, containerName)
        if(tmp.length() > 0) {
            if(!isEmpty) text.append(" ")
            text.append('''roty(«tmp»)''')
            isEmpty = false
        }

        tmp = value(rot.z, JFField.RZ, containerName)
        if(tmp.length() > 0) {
            if(!isEmpty) text.append(" ")
            text.append('''rotz(«tmp»)''')
        }

        return text
    }

    def private dispatch motionStepsToSuccessor(PrismaticJoint joint)
    '''trz(«joint.variableName»)'''
    def private dispatch motionStepsToSuccessor(RevoluteJoint joint)
    '''rotz(«joint.variableName»)'''


    def private dispatch CharSequence value(FloatLiteral f, JFField field, String container)
    {
        if(f.value == 0.0) return "" // so no motion-element will be generated

        if( constantFolding ) {
            // this should be the text in the source document that was parsed into
            // a FloatLiteral. I want to propagate exactly the same number, without
            // truncation or scientific notation due to the conversion to float
            // and then string again
            val text = NodeModelUtils.findActualNodeFor(f).text
            // Unfortunately, for some reason the text above contains lots of
            // trailing zeros
            val text2 = if(text.contains(".")) text.replaceAll("0+$","0") else text
            return text2
        }
        return "consts." + Constants.cname(container, field)
    }

    def private dispatch CharSequence value(MultExpr expr, JFField field, String container)
    {
        return expr.mult + " " + value(expr.identifier, field, container)
    }
    def private dispatch CharSequence value(DivExpr expr, JFField field, String container)
    {
        return value(expr.identifier, field, container) + "/" + expr.div
    }
    def private dispatch CharSequence value(PlainExpr expr, JFField field, String container)
    {
        return value(expr.identifier, field, container)
    }
    def private dispatch CharSequence value(PILiteral pi, JFField field, String container)
    {
        return (if(pi.minus) "-PI" else "PI")
    }

    def private dispatch CharSequence value(ParameterLiteral p, JFField field, String container)
    {
        var String group
        if( field==JFField.TX || field==JFField.TY || field==JFField.TZ ) {
            group = Parameters::lengthsGroupName
        } else {
            group = Parameters::anglesGroupName
        }
        return '''«IF p.minus»-«ENDIF»«group».«p.varname»'''
    }

}